-- Liquibase formatted SQL
-- ChangeSet Thang:1 labels:initDB,basic-table runOnChange:true splitStatements:false

CREATE TABLE IF NOT EXISTS cart
(
    id       CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS buyer
(
    id              CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    rank            integer                DEFAULT 1,
    cart_id character varying(255),
    rank_expiration date,
    CONSTRAINT fk_buyer_cart FOREIGN KEY (cart_id) REFERENCES cart (id)
);

CREATE TABLE IF NOT EXISTS cart_item
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    cart_id            character varying(255),
    product_id         character varying(255),
    product_image      character varying(255),
    product_sku        character varying(255),
    product_attributes jsonb,
    quantity           int,
    CONSTRAINT fk_cart_item_cart FOREIGN KEY (cart_id) REFERENCES cart (id)
);

CREATE TABLE IF NOT EXISTS orders
(
    id       CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    buyer_id character varying(255),
    status   character varying(50),
    CONSTRAINT fk_cart_buyer FOREIGN KEY (buyer_id) REFERENCES buyer (id)
);

CREATE TABLE IF NOT EXISTS order_item
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    order_id            character varying(255),
    product_id         character varying(255),
    product_image      character varying(255),
    product_sku        character varying(255),
    product_attributes jsonb,
    quantity           int,
    CONSTRAINT fk_cart_item_order FOREIGN KEY (order_id) REFERENCES orders (id)
);

