package dev.vnese01.coolfash.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash.order", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.coolfash.order.repository", "dev.vnese01.common"})
@EntityScan({"dev.vnese01.common.model", "dev.vnese01.coolfash.order.model"})
public class OrderApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }
}