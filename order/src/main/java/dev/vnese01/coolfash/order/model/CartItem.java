package dev.vnese01.coolfash.order.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.GenericModel;
import dev.vnese01.coolfash.order.dto.ProductAttribute;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.JdbcTypeCode;
import org.hibernate.type.SqlTypes;

import java.util.List;

@Data
@Entity
@Table(name = "cart_item")
public class CartItem implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column(name = "product_id")
    private String productId;
    @Column(name = "product_image")
    private String productImage;
    @Column(name = "product_sku")
    private String productSku;
    @Column(name = "product_attributes")
    @JdbcTypeCode(SqlTypes.JSON)
    private List<ProductAttribute> productAttributes;
    private int quantity;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    private Cart cart;
}
