package dev.vnese01.coolfash.order.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Entity
@Table (name = "orders")
@NoArgsConstructor
@AllArgsConstructor
public class Order extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "buyer_id")
    private Buyer buyer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    @JsonBackReference
    private List<OrderItem> orderList;

}
