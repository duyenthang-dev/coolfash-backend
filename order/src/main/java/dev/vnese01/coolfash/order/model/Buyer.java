package dev.vnese01.coolfash.order.model;

import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@Entity
@Table(name = "buyer")
@NoArgsConstructor
@AllArgsConstructor
public class Buyer implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column(name = "user_id")
    private String userId;
    private int rank;
    @Column(name = "rank_expiration")
    private ZonedDateTime rankExpiration;

    @OneToOne
    @JoinColumn(name = "cart_id")
    private Cart cart;
}
