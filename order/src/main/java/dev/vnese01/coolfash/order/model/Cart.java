package dev.vnese01.coolfash.order.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Entity
@Table(name = "cart")
@NoArgsConstructor
@AllArgsConstructor
public class Cart implements GenericModel<String> {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private String id;

    @JsonBackReference
    @OneToMany(mappedBy = "cart")
    private List<CartItem> items;
}
