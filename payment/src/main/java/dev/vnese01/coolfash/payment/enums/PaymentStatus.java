package dev.vnese01.coolfash.payment.enums;

public enum PaymentStatus {
    PENDING,
    SUCCESS,
    FAILED,
    CANCELLED
}
