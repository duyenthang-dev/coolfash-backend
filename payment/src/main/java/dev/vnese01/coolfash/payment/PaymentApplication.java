package dev.vnese01.coolfash.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash.payment", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.coolfash.payment.repository", "dev.vnese01.common"})
@EntityScan({"dev.vnese01.common.model", "dev.vnese01.coolfash.payment.model"})
public class PaymentApplication {
    public static void main(String[] args) {
        SpringApplication.run(PaymentApplication.class, args);
    }
}