package dev.vnese01.coolfash.payment.model;

import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "invoice")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Invoice extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String status;
    @Column(name = "order_code")
    private String orderCode;
}
