package dev.vnese01.coolfash.payment.enums;

public enum PaymentMethod {
    COD,
    VNPAY,
    MOMO
}
