-- Liquibase formatted SQL
-- ChangeSet Thang:1 labels:initDB,basic-table runOnChange:true splitStatements:false

CREATE TABLE IF NOT EXISTS invoice
(
    id       CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    status  int DEFAULT -1,
    order_code character varying(255)
);

CREATE TABLE IF NOT EXISTS payment(
     id       CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
     invoice_id character varying(255),
     amount int,
     payment_method character varying(255),
     status character varying(50),
     created_date       TIMESTAMP(6) WITH TIME ZONE,
     last_modified_date TIMESTAMP(6) WITH TIME ZONE,
     CONSTRAINT fk_payment_invoice FOREIGN KEY (invoice_id) REFERENCES invoice (id)
);
