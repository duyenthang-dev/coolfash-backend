CREATE TABLE IF NOT EXISTS review
(
    id       CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    buyer_id character varying(255),
    content  text,
    rating   int
);

CREATE TABLE IF NOT EXISTS review_media
(
    id        CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    review_id character varying(255),
    url       character varying(512),
    CONSTRAINT fk_review_media_review FOREIGN KEY (review_id) REFERENCES review (id)
);