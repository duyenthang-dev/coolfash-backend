package dev.vnese01.coolfash.review.model;

import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@Table(name = "review")
@AllArgsConstructor
@NoArgsConstructor
public class Review extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column(name = "buyer_id")
    private String buyerId;
    private String content;
    private int rating;

    @OneToMany(mappedBy = "review", fetch = FetchType.LAZY)
    private List<ReviewMedia> reviewMediaList;
}
