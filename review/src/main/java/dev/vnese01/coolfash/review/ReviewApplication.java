package dev.vnese01.coolfash.review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.coolfash.review.repository", "dev.vnese01.common"})
@EntityScan({"dev.vnese01.common.model", "dev.vnese01.coolfash.review.model"})
public class ReviewApplication {
    public static void main(String[] args) {
        SpringApplication.run(ReviewApplication.class, args);
    }
}
