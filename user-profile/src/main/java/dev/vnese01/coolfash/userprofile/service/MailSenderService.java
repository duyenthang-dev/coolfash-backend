package dev.vnese01.coolfash.userprofile.service;

import jakarta.mail.MessagingException;
import org.thymeleaf.context.Context;

public interface MailSenderService {
    void sendMail(String recipient, String subject, String body);
    void sendMailWithHTMLTemplate(String recipient, String subject, String templateName, Context ctx) throws MessagingException;
}
