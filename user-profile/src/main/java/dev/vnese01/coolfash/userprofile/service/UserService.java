package dev.vnese01.coolfash.userprofile.service;

import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.userprofile.dto.RegisterReq;
import dev.vnese01.coolfash.userprofile.model.User;
import dev.vnese01.coolfash.userprofile.repository.UserRepository;

public interface UserService extends CommonService<String, User, UserRepository> {
    User register(RegisterReq request);
    User loadByEmail(String email);
}
