package dev.vnese01.coolfash.userprofile.controller;

import dev.vnese01.common.annotation.ControllerManagedResource;
import dev.vnese01.common.context.JwtContext;
import dev.vnese01.common.controller.CrudController;
import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.dto.UserRoleDto;
import dev.vnese01.common.ex.ServiceProcessingException;
import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.userprofile.dto.RegisterReq;
import dev.vnese01.coolfash.userprofile.dto.UserResponseDto;
import dev.vnese01.coolfash.userprofile.model.User;
import dev.vnese01.coolfash.userprofile.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/users")
@ControllerManagedResource(value = "user")
public class UserController implements CrudController<String, User, RegisterReq> {
    @Override
    public Converter<User, ?> getEntityToDtoConverter() {
        return user -> {
            var userDetailsResponse = new UserResponseDto();
            BeanUtils.copyProperties(user, userDetailsResponse);
            userDetailsResponse.setRoles(user.getRoles().stream()
                    .map(r -> {
                        var role = new UserRoleDto();
                        role.setId(r.getId());
                        role.setName(r.getName());
                        return role;
                    })
                    .collect(Collectors.toSet()));
            return userDetailsResponse;
        };
    }

    @Autowired
    private UserService userService;
    @PostMapping("/register")
    public GeneralApiResponse<String> register(@RequestBody RegisterReq request) {
        User result = userService.register(request);
        if (result != null) {
            var response = GeneralApiResponse.createSuccessResponse("Register_successfully");
            return response;
        }
        return GeneralApiResponse.createErrorResponse("unknown_error", "Đã có lỗi hệ thống!!!");
    }

    @GetMapping("/by-email/{email}")
    public GeneralApiResponse<User> loadUserByEmail(@PathVariable String email) {
        User result = userService.loadByEmail(email);
        if (result != null) {
            return GeneralApiResponse.createSuccessResponse(result);
        }
        else throw new ServiceProcessingException("Unknown_error");
    }

    @GetMapping("/me")
    public GeneralApiResponse<User> getMe() {
        JwtTokenDto jwtTokenDto = JwtContext.getInstance().getJwt();
        String email = jwtTokenDto.getEmail();
        User result = userService.loadByEmail(email);
        if (result != null) {
            return GeneralApiResponse.createSuccessResponse(result);
        }
        else throw new ServiceProcessingException("Unknown_error");
    }

    @Override
    public CommonService<String, User, ?> getHandlingService() {
        return userService;
    }

}
