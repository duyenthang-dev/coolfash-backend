package dev.vnese01.coolfash.userprofile.repository;

import dev.vnese01.common.repository.CommonResourceRepository;
import dev.vnese01.coolfash.userprofile.model.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CommonResourceRepository<User, String>{
    Optional<User> findByUsernameOrEmail(String username, String email);

}
