package dev.vnese01.coolfash.userprofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl", dateTimeProviderRef = "dateTimeProvider")
@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.common.repository", "dev.vnese01.coolfash.userprofile.repository"})
@EnableFeignClients(basePackages = {"dev.vnese01.common"})
@EntityScan({"dev.vnese01.common.model", "dev.vnese01.coolfash.userprofile.model"})
public class UserProfileApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserProfileApplication.class, args);
    }
}