package dev.vnese01.coolfash.userprofile.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode(callSuper = true, exclude = "user")
@Data
@Entity
@ToString(exclude = "user")
@Table (name = "address")
public class Address extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String street;
    private String ward;
    private String district;
    private String country;
    private int zipcode;
    @Column(name = "represent_name")
    private String representName;
    @Column (name = "phone_number")
    private String phoneNumber;
    @Column (name = "is_default")
    private boolean isDefault;

    @ManyToOne (fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    // json ignore mean that we don't return this field in response inorder to avoid inf recursive data
    private User user;
}
