package dev.vnese01.coolfash.userprofile.controller;

import dev.vnese01.common.annotation.ControllerManagedResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/permission")
@ControllerManagedResource(value = "permission")
public class PermissionController {
    @GetMapping("/by-role/{roleId}")
    public Object loadPermissionByRole(@PathVariable String roleId) {
        return null;
    }

}
