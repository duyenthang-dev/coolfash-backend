package dev.vnese01.coolfash.userprofile.dto;

import dev.vnese01.common.dto.UserRoleDto;
import dev.vnese01.common.enums.Gender;
import dev.vnese01.common.model.Role;
import dev.vnese01.coolfash.userprofile.model.Address;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponseDto {
    private String id;
    private String email;
    private String avatar;
    private Set<UserRoleDto> roles;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private LocalDate birthday;
    private Gender gender;
    private Set<Address> addressList;
    private boolean active;
}
