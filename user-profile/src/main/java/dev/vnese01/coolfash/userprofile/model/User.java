package dev.vnese01.coolfash.userprofile.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import dev.vnese01.common.enums.Gender;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import dev.vnese01.common.model.Role;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "users")
@Data
public class User extends BaseEntity implements GenericModel<String>{
    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String avatar;
    @Column (name = "first_name")
    private String firstName;
    @Column (name = "last_name")
    private String lastName;
    private String email;
    private String username;
    @Column(name = "phone_number")
    private String phoneNumber;
    private String password;
    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    @JsonManagedReference
    @Fetch(FetchMode.SUBSELECT)
    Set<Role> roles = new HashSet<>();

    @OneToMany (mappedBy = "user", fetch = FetchType.LAZY)
    @Fetch(FetchMode.SUBSELECT)
    private Set<Address> addressList = new HashSet<>();
}
