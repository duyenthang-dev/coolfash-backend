package dev.vnese01.coolfash.userprofile.service.impl;

import dev.vnese01.common.enums.UserType;
import dev.vnese01.common.ex.ResourceNotFound;
import dev.vnese01.common.ex.ServiceProcessingException;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.coolfash.userprofile.dto.RegisterReq;
import dev.vnese01.common.model.Role;
import dev.vnese01.coolfash.userprofile.model.User;
import dev.vnese01.coolfash.userprofile.repository.UserRepository;
import dev.vnese01.coolfash.userprofile.service.MailSenderService;
import dev.vnese01.coolfash.userprofile.service.UserService;
import jakarta.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MailSenderService mailSenderService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
    @Override
    public User register(RegisterReq request) {
        String email = request.getEmail();
        Optional<User> optionalUser = userRepository.findByUsernameOrEmail(email, email);
        if (optionalUser.isPresent()) {
            throw new ServiceProcessingException("Email is already taken");
        }
        User newUser = new User();
        BeanUtils.copyProperties(request, newUser);
        newUser.setRoles(request.getRoles().stream().map(userType -> {
            Role role = new Role();
            role.setId(userType.name());
            return role;
        }).collect(Collectors.toSet()));
        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
        newUser = userRepository.save(newUser);
        try {
            String res = sendVerifyMail(newUser.getFirstName() + " " + newUser.getLastName(), newUser.getEmail());
            System.out.println(res);
        } catch (MessagingException e) {
            throw new ServiceProcessingException("Verification email could not be send to new user");
        }
        return newUser;
    }

    @Override
    public User loadByEmail(String email) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        var authorities = authentication.getAuthorities();
        authorities.forEach(authority -> System.out.println(authority.getAuthority()));
        Optional<User> optionalUser= userRepository.findByUsernameOrEmail(email, email);
        return optionalUser.orElseThrow(() -> new ResourceNotFound("User not found!!"));
    }

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    @Override
    public <D extends CreateUpdateDto<String>> void copyAdditionalPropsFromDtoToEntity(D dto, User entity) {
        RegisterReq registerReq = (RegisterReq) dto;
        String pwd = registerReq.getPassword();
        if (pwd != null) {
            entity.setPassword(passwordEncoder.encode(pwd));
        }
        if (registerReq.getAvatar() == null) {
            entity.setAvatar("https://res.cloudinary.com/master-dev/image/upload/v1657251169/ChatApp/uploads/avatar/default-avatar_glrb8q.png");
        }
        if (registerReq.getRoles() == null) {
            Role role = new Role();
            role.setId("BUYER");
            entity.setRoles(Set.of(role));
        }
        else {
            entity.setRoles(registerReq.getRoles().stream().map(userType -> {
                Role role = new Role();
                role.setId(userType.name());
                return role;
            }).collect(Collectors.toSet()));
        }
    }

    private String sendVerifyMail(String fullName, String email) throws MessagingException {
        Context ctx = new Context();
        ctx.setVariable("fullName", fullName);
        ctx.setVariable("link", "http://localhost:3000/verify-mail");
        String recipient = email;
        String subject = "Xác thực email";
        String templateName = "verify-email";
        mailSenderService.sendMailWithHTMLTemplate(recipient, subject, templateName, ctx);
        return "An email has been sent to your email. Check and verify it";
    }

}
