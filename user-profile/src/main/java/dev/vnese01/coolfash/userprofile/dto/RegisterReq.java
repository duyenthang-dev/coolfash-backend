package dev.vnese01.coolfash.userprofile.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import dev.vnese01.common.enums.Gender;
import dev.vnese01.common.enums.UserType;
import dev.vnese01.common.model.CreateUpdateDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterReq implements CreateUpdateDto<String> {
    private String id;
    private String email;
    private String avatar;
    private String password;
    private List<UserType> roles;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private LocalDate birthday;
    private Gender gender;
}
