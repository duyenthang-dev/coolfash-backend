-- Liquibase formatted SQL
-- ChangeSet Thang:2 labels:addData,basic-table runOnChange:true splitStatements:false
-- ADD USER
INSERT INTO users (id, first_name, last_name, email, username, phone_number, password, birthday, gender, created_date,
                   last_modified_date)
VALUES ('5ff77c99-a49a-4ae5-9bdb-bbe21d931ff9', 'Hà Duyên', 'Thắng', 'duyenthang.dev@gmail.com', 'duyenthang-dev',
        '0988777666', '$2y$10$feF08Vv2Kygwywv.3rKGl.bb6i8.sV/vCPrgO8zbYKKoZp6aw5nju', '2001-05-06', 'MALE', NULL, NULL);

INSERT INTO users (id, first_name, last_name, email, username, phone_number, password, birthday, gender, created_date,
                   last_modified_date)
VALUES ('d6048a45-ae26-4e27-b803-3a95a4b2c680', 'Buyer', 'One', 'buyerone@gmail.com', 'buyer-one',
        '0999123456', '$2y$10$8iy808hqevYR8og5ruYAe.pQe5t8U5GIzaMk1rY0A9Vjdj728S/72', '2002-10-06', 'MALE', NULL, NULL);

INSERT INTO users (id, first_name, last_name, email, username, phone_number, password, birthday, gender, created_date,
                   last_modified_date)
VALUES ('a9442a68-86a5-40ef-9097-a3fadcefa60e', 'Seller', 'One', 'sellerone@gmail.com', 'seller-one',
        '0999123457', '$2y$10$y6GpE0p5LpDkbNIwgDtJfuL2yvg/pY/Rne/a.UF0gqODyuQt/7VZi', '2000-12-16', 'FEMALE', NULL, NULL);
-- ADD ROLE

INSERT INTO role (id, name, description, created_date, last_modified_date)
VALUES ('SUPERADMIN', 'SUPERADMIN', 'Super Admin, manage every thing', CURRENT_DATE, CURRENT_DATE);

INSERT INTO role (id, name, description, created_date, last_modified_date)
VALUES ('ADMIN', 'ADMIN', 'Admin, manage every thing', CURRENT_DATE, CURRENT_DATE);

INSERT INTO role (id, name, description, created_date, last_modified_date)
VALUES ('BUYER', 'BUYER', 'BUYER', CURRENT_DATE, CURRENT_DATE);

INSERT INTO role (id, name, description, created_date, last_modified_date)
VALUES ('SELLER', 'SELLER', 'SELLER', CURRENT_DATE, CURRENT_DATE);

-- ADD PERMISSION
INSERT INTO permission (id, name, description, request_method, resource, url, action, created_date, last_modified_date)
VALUES ('affef6fa-3630-4850-883c-ab767a7dd153', NULL, NULL, 'GET', 'product', '/products*', 'ALLOWED', CURRENT_DATE,
        CURRENT_DATE);

INSERT INTO permission (id, name, description, request_method, resource, url, action, created_date, last_modified_date)
VALUES ('ef7a8775-d9dc-467f-a31e-237240912222', NULL, NULL, 'POST', 'order', '/order/create', 'DENY', CURRENT_DATE,
        CURRENT_DATE);

INSERT INTO permission (id, name, description, request_method, resource, url, action, created_date, last_modified_date)
VALUES ('2147a6e4-743c-4c92-96f3-384f911b092c', NULL, NULL, 'ANY', 'user', '/admin*', 'DENY', CURRENT_DATE,
        CURRENT_DATE);

-- ADD PIVOT TABLE
INSERT INTO users_role (id, role_id, user_id)
VALUES ('19a6baaa-2761-4ad5-ab25-91ae341547e5', 'SUPERADMIN', '5ff77c99-a49a-4ae5-9bdb-bbe21d931ff9');

INSERT INTO users_role (id, role_id, user_id)
VALUES ('c52a5cff-50b9-4397-b659-875a370d87b4', 'BUYER', 'd6048a45-ae26-4e27-b803-3a95a4b2c680');

INSERT INTO users_role (id, role_id, user_id)
VALUES ('fbb87004-37ac-4184-80ca-eb4ed65b5d52', 'SELLER', 'a9442a68-86a5-40ef-9097-a3fadcefa60e');

INSERT INTO role_permission (id, role_id, permission_id)
VALUES ('05293c75-3f33-4b23-8b92-2b3b270b1669', 'SUPERADMIN', 'affef6fa-3630-4850-883c-ab767a7dd153');

INSERT INTO role_permission (id, role_id, permission_id)
VALUES ('99b3b179-6f72-45c7-9f71-ab31205f56ef', 'BUYER', 'affef6fa-3630-4850-883c-ab767a7dd153');

INSERT INTO role_permission (id, role_id, permission_id)
VALUES ('088f59e9-4057-466f-a7b8-7f5343769aeb', 'SELLER', 'affef6fa-3630-4850-883c-ab767a7dd153');

INSERT INTO permission (id, name, description, request_method, resource, url, action, created_date, last_modified_date)
VALUES ('1963c196-f2ee-4581-949d-29c56675c5b4', NULL, NULL, 'ANY', 'user', '/users/by-email*', 'DENY', NULL, NULL);

INSERT INTO role_permission (id, role_id, permission_id)
VALUES ('08f58365-cee3-4210-b01c-8d059177079a', 'BUYER', '1963c196-f2ee-4581-949d-29c56675c5b4');

INSERT INTO address (id, user_id, street, ward, district, city, country, zipcode, represent_name, phone_number,
                     is_default,
                     created_date, last_modified_date)
VALUES ('a9442a68-86a5-40ef-9097-a3fadcefa60e', '5ff77c99-a49a-4ae5-9bdb-bbe21d931ff9', '498 Huỳnh Tấn Phát',
        'Bình Thuận', 'Quận 7', 'Tp.Hồ Chí Minh', 'Việt Nam', 70000, 'Hà Duyên Thắng',
        '0999111222', TRUE, CURRENT_DATE, CURRENT_DATE);

INSERT INTO address (id, user_id, street, ward, district, city, country, zipcode, represent_name, phone_number,
                     is_default,
                     created_date, last_modified_date)
VALUES ('47d8e009-62aa-47d4-bd4a-503170922370', 'd6048a45-ae26-4e27-b803-3a95a4b2c680', '686 Tên Lửa',
        'Bình Trị Đông B', 'Bình Tân', 'Tp.Hồ Chí Minh', 'Việt Nam', 72000, 'Nguyễn Văn A',
        '0998111222', TRUE, CURRENT_DATE, CURRENT_DATE);

INSERT INTO address (id, user_id, street, ward, district, city, country, zipcode, represent_name, phone_number,
                     is_default,
                     created_date, last_modified_date)
VALUES ('2cfb8b41-da93-4a87-aa3d-5688cf6cb8ef', 'a9442a68-86a5-40ef-9097-a3fadcefa60e', '117/6 Ba Vân',
        'P.14', 'Tân Bình', 'Tp.Hồ Chí Minh', 'Việt Nam', 74000, 'Lê Thị B',
        '0990111222', TRUE, CURRENT_DATE, CURRENT_DATE);

-- /ChangeSet