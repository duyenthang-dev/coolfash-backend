-- Liquibase formatted SQL
-- ChangeSet Thang:1 labels:initDB,basic-table runOnChange:true splitStatements:false
CREATE TABLE users
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    avatar             CHARACTER VARYING(1024) DEFAULT 'https://res.cloudinary.com/master-dev/image/upload/v1657251169/ChatApp/uploads/avatar/default-avatar_glrb8q.png',
    first_name         CHARACTER VARYING(255),
    last_name          CHARACTER VARYING(255),
    email              CHARACTER VARYING(255) UNIQUE NOT NULL ,
    username           CHARACTER VARYING(255),
    phone_number       CHARACTER VARYING(10),
    password           text,
    birthday           DATE,
    gender             CHARACTER VARYING(10),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS role
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    name               VARCHAR(255),
    description        VARCHAR(512),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS users_role
(
    id      CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    role_id VARCHAR(255),
    user_id VARCHAR(255),
    CONSTRAINT fk_user_role_user FOREIGN KEY (user_id) REFERENCES users (id),
    CONSTRAINT fk_user_role_role FOREIGN KEY (role_id) REFERENCES role (id)
);

CREATE TABLE IF NOT EXISTS permission
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    name               VARCHAR(100),
    description        VARCHAR(1024),
    request_method     VARCHAR(512),
    resource           VARCHAR(512),
    url                VARCHAR(512),
    action             VARCHAR(100),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS role_permission
(
    id            CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    role_id       VARCHAR(255),
    permission_id VARCHAR(255),
    CONSTRAINT fk_role_permission_role FOREIGN KEY (role_id) REFERENCES role (id),
    CONSTRAINT fk_role_permission_permission FOREIGN KEY (permission_id) REFERENCES permission (id)
);

CREATE TABLE IF NOT EXISTS address
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    user_id            VARCHAR(255),
    street             VARCHAR(255),
    ward               VARCHAR(255),
    district           VARCHAR(255),
    city               VARCHAR(128),
    country            VARCHAR(128),
    zipcode            INT,
    represent_name     VARCHAR(100),
    phone_number       VARCHAR(10),
    is_default         BOOLEAN                DEFAULT TRUE,
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN DEFAULT TRUE,
    CONSTRAINT fk_address_user FOREIGN KEY (user_id) REFERENCES users (id)
);
-- /ChangeSet



