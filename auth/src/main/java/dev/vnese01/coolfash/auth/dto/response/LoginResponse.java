package dev.vnese01.coolfash.auth.dto.response;

import dev.vnese01.common.dto.UserFeignResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginResponse {
    private String token;
    private String refreshToken;
    private UserFeignResponse user;
}
