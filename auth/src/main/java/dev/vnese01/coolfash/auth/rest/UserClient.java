package dev.vnese01.coolfash.auth.rest;

import dev.vnese01.common.annotation.ManualPermissionCheck;
import dev.vnese01.common.config.GenericFeignConfig;
import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.dto.UserFeignResponse;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${feign.user.name}", url = "${feign.user.url}", configuration = GenericFeignConfig.class)
@ConditionalOnProperty(prefix = "feign.user", name = "name")
public interface UserClient {
    @GetMapping("/users/by-email/{email}")
    @ManualPermissionCheck()
    GeneralApiResponse<UserFeignResponse> loadUserByEmail(@PathVariable String email);
}
