package dev.vnese01.coolfash.auth.service;

import dev.vnese01.coolfash.auth.dto.request.LoginRequest;
import dev.vnese01.coolfash.auth.dto.response.LoginResponse;

public interface AuthService {
    LoginResponse login (LoginRequest request);
}
