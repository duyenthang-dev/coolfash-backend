package dev.vnese01.coolfash.auth.controller;

import dev.vnese01.common.annotation.ManualPermissionCheck;
import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.coolfash.auth.dto.request.LoginRequest;
import dev.vnese01.coolfash.auth.dto.response.LoginResponse;
import dev.vnese01.coolfash.auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @PostMapping("/login")
    @ManualPermissionCheck()
    public GeneralApiResponse<LoginResponse> login(@RequestBody LoginRequest request) {
        var response = authService.login(request);
        return GeneralApiResponse.createSuccessResponse(response);
    }
}
