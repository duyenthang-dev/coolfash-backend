package dev.vnese01.coolfash.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl", dateTimeProviderRef = "dateTimeProvider")
@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.coolfash.auth", "dev.vnese01.common"})
@EnableFeignClients(basePackages = {"dev.vnese01.common", "dev.vnese01.coolfash.auth" })
@EntityScan({"dev.vnese01.common.model"})
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);
    }
}