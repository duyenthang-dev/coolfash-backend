package dev.vnese01.coolfash.auth.service.impl;

import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.dto.UserFeignResponse;
import dev.vnese01.common.model.UserDetailImpl;
import dev.vnese01.coolfash.auth.rest.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CFSUserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserClient userClient;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        GeneralApiResponse<UserFeignResponse> res = userClient.loadUserByEmail(username);
        UserFeignResponse user = res.getResult();
        return UserDetailImpl.buildUser(user);

    }

//    private Set<GrantedAuthority> convertAuthorities(Set<Role> roleSet) {
//        Set<GrantedAuthority> authorities = new HashSet<>();
//        for (Role role: roleSet) {
//            authorities.add(new SimpleGrantedAuthority(role.getName()));
//        }
//        return authorities;
//    }
}
