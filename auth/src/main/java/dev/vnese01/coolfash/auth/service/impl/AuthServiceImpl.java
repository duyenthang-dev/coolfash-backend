package dev.vnese01.coolfash.auth.service.impl;

import dev.vnese01.common.context.JwtContext;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.dto.UserFeignResponse;
import dev.vnese01.common.service.JwtService;
import dev.vnese01.coolfash.auth.dto.request.LoginRequest;
import dev.vnese01.coolfash.auth.dto.response.LoginResponse;
import dev.vnese01.common.model.UserDetailImpl;
import dev.vnese01.coolfash.auth.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AuthenticationManager authManager;
    @Autowired
    private JwtService jwtService;
    @Override
    @Transactional(readOnly = true)
    public LoginResponse login(LoginRequest request) {
        var response = new LoginResponse();
        Authentication auth = authManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
        UserDetailImpl userDetail = (UserDetailImpl) auth.getPrincipal();
        SecurityContextHolder.getContext().setAuthentication(auth);
        UserFeignResponse user = userDetail.getUser();
        JwtTokenDto jwtTokenDto = buildTokenDto(user);
        String accessToken = jwtService.buildAccessToken(jwtTokenDto);
        String refreshToken = jwtService.buildRefreshToken(jwtTokenDto, accessToken);
        response.setToken(accessToken);
        response.setRefreshToken(refreshToken);
        response.setUser(user);
        JwtContext.getInstance().setJwt(jwtTokenDto);
        return response;
    }

    private JwtTokenDto buildTokenDto(UserFeignResponse user) {
        JwtTokenDto tokenDto = new JwtTokenDto();
        tokenDto.setUserId(user.getId());
        tokenDto.setEmail(user.getEmail());
        tokenDto.setUsername(user.getUsername());
        tokenDto.setRoles(new ArrayList<>(user.getRoles()));
        return tokenDto;
    }
}
