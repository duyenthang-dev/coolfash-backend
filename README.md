# CoolFash - An E-commerce Microservices Application
## Solution Overview
Coolfash is a robust e-commerce application built on a microservices architecture using Spring technologies and other open-source tools. <br >
Coolfash bings benefits for whole buyer and seller.
- Buyer can easily register account and browse for items.
- Quick purchase process is highlighted, saving time compared to offline shopping.
- Seller can create their booths, manage merchandises, order and much more. Coolfash helps seller increase control and efficiency in managing their online business
## Technology 
- Leverages the power of Spring Boot, Netflix Eureka, Spring Cloud Gateway, OAtuth2 ... for service development, discovery, gateway management, and security, respectively.
- Using Prometheus and Grafana for monitoring and visualization.
## Solution Architecture
<img src="https://i.ibb.co/z6JpMh8/coolfash-sa.png" alt="coolfash-sa" border="0">
## Getting Started
### Prerequisites
- Installed Docker and Docker Compose
### Deployment
Test push
...
