package dev.vnese01.coolfash.product.service;

import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.product.model.Product;
import dev.vnese01.coolfash.product.repository.ProductRepository;

public interface ProductService extends CommonService<String, Product, ProductRepository> {
}
