package dev.vnese01.coolfash.product.dto;

import dev.vnese01.coolfash.product.model.ProductVariant;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductVariantDto {
    private String id;
    private String sku;
    private int quantity;
    private String name;
    private Double discountPrice;
    private double price;
    private String image;

    public static ProductVariantDto fromEntity(ProductVariant entity) {
        ProductVariantDto dto = new ProductVariantDto();
        dto.setId(entity.getId());
        dto.setSku(entity.getSku());
        dto.setQuantity(entity.getQuantity());
        dto.setName(entity.getName());
        dto.setDiscountPrice(entity.getDiscountPrice());
        dto.setPrice(entity.getPrice());
        dto.setImage(entity.getImage());
        return dto;
    }
}
