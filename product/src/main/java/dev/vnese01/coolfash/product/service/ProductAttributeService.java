package dev.vnese01.coolfash.product.service;

import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.product.model.ProductAttribute;
import dev.vnese01.coolfash.product.repository.ProductAttributeRepository;

import java.util.List;

public interface ProductAttributeService extends CommonService<String, ProductAttribute, ProductAttributeRepository> {
    List<ProductAttribute> addMultipleAttribute(List<ProductAttribute> attributes);
}
