package dev.vnese01.coolfash.product.repository;

import dev.vnese01.common.repository.CommonResourceRepository;
import dev.vnese01.coolfash.product.model.ProductAttribute;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductAttributeRepository extends CommonResourceRepository<ProductAttribute,String> {
}
