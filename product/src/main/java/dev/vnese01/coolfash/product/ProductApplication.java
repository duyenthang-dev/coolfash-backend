package dev.vnese01.coolfash.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"dev.vnese01.coolfash", "dev.vnese01.common"})
@EnableJpaRepositories(basePackages = {"dev.vnese01.coolfash.product.repository", "dev.vnese01.common"})
@EntityScan({"dev.vnese01.common.model", "dev.vnese01.coolfash.product.model"})
@EnableJpaAuditing(auditorAwareRef = "auditAwareImpl", dateTimeProviderRef = "dateTimeProvider")
public class ProductApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }
}