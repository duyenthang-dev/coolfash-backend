package dev.vnese01.coolfash.product.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.List;

@Entity
@Table(name = "product")
@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
public class Product extends BaseEntity implements GenericModel<String>, CreateUpdateDto<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String name;
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonBackReference
    private List<ProductVariant> productVariants;

    @OneToMany(fetch =  FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonBackReference
    private List<ProductCategory> categories;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "merchant_id")
    @Fetch(FetchMode.JOIN)
    @JsonManagedReference
    private Merchant merchant;
}
