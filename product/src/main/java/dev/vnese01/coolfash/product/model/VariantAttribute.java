package dev.vnese01.coolfash.product.model;

import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table (name = "variant_attribute")
public class VariantAttribute implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String value;

    @ManyToOne(fetch =  FetchType.LAZY)
    @JoinColumn(name = "attribute_id")
    private ProductAttribute productAttribute;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "variant_id")
    private ProductVariant productVariant;
}
