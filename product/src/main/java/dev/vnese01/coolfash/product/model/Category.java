package dev.vnese01.coolfash.product.model;

import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "category")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Category extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String name;

    @OneToOne
    @JoinColumn(name = "parent_id")
    private Category parent;
}
