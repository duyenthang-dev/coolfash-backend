package dev.vnese01.coolfash.product.controller;

import dev.vnese01.common.annotation.ControllerManagedResource;
import dev.vnese01.common.controller.CrudController;
import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.product.dto.ProductVariantDto;
import dev.vnese01.coolfash.product.model.ProductVariant;
import dev.vnese01.coolfash.product.service.ProductVariantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/product-variants")
@ControllerManagedResource(value = "product_variants")
public class ProductVariantController implements CrudController<String, ProductVariant, ProductVariant> {
    @Autowired
    private ProductVariantService productVariantService;

    @GetMapping("/test")
    public GeneralApiResponse<?> test() {
        return GeneralApiResponse.createSuccessResponse("OK");
    }

    @GetMapping("/product/{id}")
    public GeneralApiResponse<List<ProductVariantDto>> getVariantByProductId(@PathVariable String id) {
        var result = productVariantService.getProductVariantsByProductId(id);
        return GeneralApiResponse.createSuccessResponse(result);
    }

    @Override
    public CommonService<String, ProductVariant, ?> getHandlingService() {
        return productVariantService;
    }
}
