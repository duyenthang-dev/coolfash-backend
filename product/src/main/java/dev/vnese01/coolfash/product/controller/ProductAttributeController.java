package dev.vnese01.coolfash.product.controller;

import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.coolfash.product.model.ProductAttribute;
import dev.vnese01.coolfash.product.service.ProductAttributeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/product-attributes")
@RequiredArgsConstructor
public class ProductAttributeController {
    private final ProductAttributeService productAttributeService;

    @PostMapping(path = "/create")
    public GeneralApiResponse<List<ProductAttribute>> addAttributes(@RequestParam List<ProductAttribute> attributes) {
        return GeneralApiResponse.createSuccessResponse(productAttributeService.addMultipleAttribute(attributes));
    }
}
