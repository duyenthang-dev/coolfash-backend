package dev.vnese01.coolfash.product.service.impl;

import dev.vnese01.coolfash.product.model.ProductAttribute;
import dev.vnese01.coolfash.product.repository.ProductAttributeRepository;
import dev.vnese01.coolfash.product.service.ProductAttributeService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductAttributeServiceImpl implements ProductAttributeService {
    private final ProductAttributeRepository productAttributeRepository;

    @Override
    public ProductAttributeRepository getRepository() {
        return productAttributeRepository;
    }

    @Override
    public List<ProductAttribute> addMultipleAttribute(List<ProductAttribute> attributes) {
        return productAttributeRepository.saveAll(attributes);
    }
}
