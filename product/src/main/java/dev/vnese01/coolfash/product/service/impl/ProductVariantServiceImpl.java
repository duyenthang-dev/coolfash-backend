package dev.vnese01.coolfash.product.service.impl;

import dev.vnese01.coolfash.product.dto.ProductVariantDto;
import dev.vnese01.coolfash.product.repository.ProductVariantRepository;
import dev.vnese01.coolfash.product.service.ProductVariantService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductVariantServiceImpl implements ProductVariantService {
    private final ProductVariantRepository productVariantRepository;
    @Override
    public ProductVariantRepository getRepository() {
        return productVariantRepository;
    }

    @Override
    public List<ProductVariantDto> getProductVariantsByProductId(String productId) {
        List<ProductVariantDto> result = productVariantRepository.findAllByProductId(productId)
                .stream().map(ProductVariantDto::fromEntity)
                .collect(Collectors.toCollection(ArrayList::new));
        return result;
    }
}
