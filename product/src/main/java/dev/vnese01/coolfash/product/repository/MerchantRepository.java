package dev.vnese01.coolfash.product.repository;

import dev.vnese01.common.repository.CommonResourceRepository;
import dev.vnese01.coolfash.product.model.Merchant;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantRepository extends CommonResourceRepository<Merchant, String> {
}
