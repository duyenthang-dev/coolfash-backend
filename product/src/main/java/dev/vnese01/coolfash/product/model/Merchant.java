package dev.vnese01.coolfash.product.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "merchant")
@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class Merchant extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    @Column(name = "store_name")
    private String storeName;
    private String url;
    private String description;
    @Column(name = "avg_rating")
    private double avgRating;
    @Column(name = "rating_count")
    private int ratingCount;
    @Column(name = "user_id")
    private String userId;

    @OneToMany(fetch =  FetchType.LAZY, mappedBy = "merchant")
    @JsonBackReference
    private List<Product> product;
}
