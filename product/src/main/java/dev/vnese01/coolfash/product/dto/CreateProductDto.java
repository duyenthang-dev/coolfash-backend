package dev.vnese01.coolfash.product.dto;

import dev.vnese01.common.model.CreateUpdateDto;
import lombok.*;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateProductDto implements CreateUpdateDto<String> {
    private String id;
    private String name;
    private String description;
    private String merchantId;
    private List<String> categoriesId;
    private List<ProductVariant> attributes;
    private DeliveryInfo deliveryInfo;

    record ProductVariant(String sku, int quantity, double price, Double discountPrice, List<VariantAttribute> productAttributes){}
    record VariantAttribute(String name, String key) {}
    record DeliveryInfo(Double weight, Double width, Double height, Double length, String deliveryProvider){}
}
