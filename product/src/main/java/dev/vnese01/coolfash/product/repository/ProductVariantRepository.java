package dev.vnese01.coolfash.product.repository;

import dev.vnese01.common.repository.CommonResourceRepository;
import dev.vnese01.coolfash.product.model.ProductVariant;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductVariantRepository extends CommonResourceRepository<ProductVariant, String> {
    List<ProductVariant> findAllByProductId(String productId);
}
