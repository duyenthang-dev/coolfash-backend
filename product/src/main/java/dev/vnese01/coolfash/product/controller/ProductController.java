package dev.vnese01.coolfash.product.controller;

import dev.vnese01.common.annotation.ControllerManagedResource;
import dev.vnese01.common.controller.CrudController;
import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.product.dto.CreateProductDto;
import dev.vnese01.coolfash.product.dto.GetProductResponse;
import dev.vnese01.coolfash.product.model.Product;
import dev.vnese01.coolfash.product.model.ProductVariant;
import dev.vnese01.coolfash.product.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.stream.Collectors;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "/products")
@ControllerManagedResource(value = "product")
public class ProductController implements CrudController<String, Product, CreateProductDto> {
    @Autowired
    private ProductService productService;

    @Override
    public Converter<Product, ?> getEntityToDtoConverter() {
        return product -> {
            var productDto = new GetProductResponse();
            BeanUtils.copyProperties(product, productDto);
            var variants = product.getProductVariants();
            if (variants == null) {
                variants = new ArrayList<>();
            }
            ProductVariant newestVariant = variants.stream()
                    .sorted((a, b) -> b.getLastModifiedDate().compareTo(a.getLastModifiedDate()))
                    .toList().get(0);
            productDto.setImage(newestVariant.getImage());
            productDto.setPrice(newestVariant.getPrice());
            var categories = product.getCategories().stream().map(pc -> pc.getCategory().getName()).collect(Collectors.joining());
            int stock = variants.stream()
                            .reduce(0, (prevTotal, elm) -> prevTotal + elm.getQuantity(), Integer::sum);
            productDto.setCategory(categories);
            productDto.setStock(stock);
            productDto.setDiscountPrice(newestVariant.getDiscountPrice());
            productDto.setMerchantName(product.getMerchant().getStoreName());
            return productDto;
        };
    }

    @Override
    public CommonService<String, Product, ?> getHandlingService() {
        return productService;
    }
}
