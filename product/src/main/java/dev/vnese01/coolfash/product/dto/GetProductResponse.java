package dev.vnese01.coolfash.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProductResponse {
    private String id;
    private String name;
    private String description;
    private String image;
    private double price;
    private Double discountPrice;
    private String category;
    private int stock;
    private int soldCount;
    private String merchantName;
}
