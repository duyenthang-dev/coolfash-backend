package dev.vnese01.coolfash.product.service;

import dev.vnese01.common.service.CommonService;
import dev.vnese01.coolfash.product.dto.ProductVariantDto;
import dev.vnese01.coolfash.product.model.ProductVariant;
import dev.vnese01.coolfash.product.repository.ProductVariantRepository;

import java.util.List;

public interface ProductVariantService extends CommonService<String, ProductVariant, ProductVariantRepository> {
    List<ProductVariantDto> getProductVariantsByProductId(String productId);
}
