package dev.vnese01.coolfash.product.service.impl;

import dev.vnese01.common.ex.ResourceNotFound;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.coolfash.product.dto.CreateProductDto;
import dev.vnese01.coolfash.product.model.*;
import dev.vnese01.coolfash.product.repository.*;
import dev.vnese01.coolfash.product.service.ProductService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final ProductVariantRepository productVariantRepository;
    private final CategoryRepository categoryRepository;
    private final MerchantRepository merchantRepository;
    private final ProductCategoryRepository productCategoryRepository;

    @Override
    public ProductRepository getRepository() {
        return productRepository;
    }

    @Override
    public <D extends CreateUpdateDto<String>> void copyAdditionalPropsFromDtoToEntity(D dto, Product entity) {
        CreateProductDto productDto = (CreateProductDto) dto;
        Optional<Merchant> merchantDb = merchantRepository.findById(productDto.getMerchantId());
        merchantDb.ifPresentOrElse(entity::setMerchant, () -> {
            throw new ResourceNotFound("Người bán không tồn tại");
        });
    }

    @Override
    public <D extends CreateUpdateDto<String>> void doAfterCreateEntity(D dto, Product entity) {
        CreateProductDto productDto = (CreateProductDto) dto;
        List<ProductVariant> productVariants = productDto.getVariants().stream().map(variantDto -> {
            var variant = new ProductVariant();
            variant.setSku(variantDto.getSku());
            variant.setName(variantDto.getName());
            variant.setQuantity(variantDto.getQuantity());
            variant.setPrice(variantDto.getPrice());
            variant.setDiscountPrice(variantDto.getDiscountPrice());
            variant.setImage(variantDto.getImage());
            variant.setProduct(entity);
            return variant;
        }).collect(Collectors.toCollection(ArrayList::new));


        entity.setProductVariants(productVariants);

        List<Category> categories = categoryRepository.findAllById(productDto.getCategoriesId());
        List<ProductCategory> productCategories = categories.stream()
                .map(category -> {
                    var productCategory = new ProductCategory();
                    productCategory.setCategory(category);
                    productCategory.setProduct(entity);
                    return productCategory;
                }).collect(Collectors.toCollection(ArrayList::new));

        entity.setCategories(productCategories);

        productVariantRepository.saveAll(productVariants);
        productCategoryRepository.saveAll(productCategories);
    }
}
