package dev.vnese01.coolfash.product.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.BaseEntity;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@Entity
@Table(name = "product_variant")
@EqualsAndHashCode(callSuper = true)
public class ProductVariant extends BaseEntity implements GenericModel<String>, CreateUpdateDto<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String sku;
    private int quantity;
    private String name;
    @Column(name = "discount_price")
    private Double discountPrice;
    private double price;
    private String image;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id")
    @JsonManagedReference
    @ToString.Exclude
    private Product product;
}
