package dev.vnese01.coolfash.product.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductVariantResponse {
    private String name;
    private String description;
    private String image;
    private double price;
    private double discountPrice;
    private int soldCount;
}
