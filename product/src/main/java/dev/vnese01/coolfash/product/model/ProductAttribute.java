package dev.vnese01.coolfash.product.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import dev.vnese01.common.model.GenericModel;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "product_attribute")
public class ProductAttribute implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String key;
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @JsonManagedReference
    private Product product;
}
