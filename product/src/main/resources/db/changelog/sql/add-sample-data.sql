-- Liquibase formatted SQL
-- ChangeSet Thang:2 labels:initDB,basic-table runOnChange:true splitStatements:false

--- Add sample merchant
INSERT INTO merchant (id, store_name, url, description, avg_rating, rating_count, user_id, created_date, last_modified_date)
VALUES ('M1', 'Bi Luxury', 'https://biluxury.vn/', 'VÌ MỘT VIỆT NAM ĐẸP HƠN', 0, 0,
        'a9442a68-86a5-40ef-9097-a3fadcefa60e', now(), now());
--- Add sample products
INSERT INTO product (id, name, description, created_date, last_modified_date, active, merchant_id)
VALUES ('P1', 'Áo sơ mi', 'Áo sơ mi, phù hợp cho môi trường công sở', '2024-01-01', '2024-01-01', TRUE, 'M1');

INSERT INTO product (id, name, description, created_date, last_modified_date, active, merchant_id)
VALUES ('P2', 'Áo thun', 'Mẫu áo ngắn tay, trẻ trung năng động', '2024-01-01', '2024-01-01', TRUE, 'M1');

INSERT INTO product (id, name, description, created_date, last_modified_date, active, merchant_id)
VALUES ('P3', 'Quần tây', 'Quần công sở', '2024-01-01', '2024-01-01', TRUE, 'M1');

INSERT INTO product (id, name, description, created_date, last_modified_date, active, merchant_id)
VALUES ('P4', 'Quần jeans', 'Quần dài, phù hợp với các hoạt động ngoài trời', '2024-01-01', '2024-01-01', TRUE, 'M1');

--- Add sample categories
INSERT INTO category (id, parent_id, name, created_date, last_modified_date)
VALUES ('C1', NULL, 'Quần', now(), now());

INSERT INTO category (id, parent_id, name, created_date, last_modified_date)
VALUES ('C2', NULL, 'Áo', now(), now());

INSERT INTO category (id, parent_id, name, created_date, last_modified_date)
VALUES ('C3', NULL, 'Phụ kiện', now(), now());

--- Add product_category
INSERT INTO product_category (id, product_id, category_id)
VALUES ('PAC1', 'P1', 'C2');

INSERT INTO product_category (id, product_id, category_id)
VALUES ('PAC2', 'P2', 'C2');

INSERT INTO product_category (id, product_id, category_id)
VALUES ('PAC3', 'P3', 'C1');

INSERT INTO product_category (id, product_id, category_id)
VALUES ('PAC4', 'P4', 'C1');

--- Add product_attribute
INSERT INTO product_attribute (id, name, key)
VALUES ('PA_1', 'Kích thươớc', 'kich_thuoc');

INSERT INTO product_attribute (id, name, key)
VALUES ('PA_2', 'Màu sắc', 'mau_sac');

--- Add product variant
INSERT INTO product_variant (id, sku, quantity, name, price, image, product_id, created_date, last_modified_date)
VALUES ('PV1', '7SMDC007XDM', 100, 'Áo Sơ Mi Dài Tay Từ Sợi Tre Mềm Mịn, Kháng Khuẩn, Chống Nhăn', 500000,
        'https://product.hstatic.net/200000053174/product/1_55928f3caa6a4ea6947deb597adfcf05_master.jpg',
        'P1', now(), now());

INSERT INTO product_variant (id, sku, quantity, name, price, image, product_id, created_date, last_modified_date)
VALUES ('PV2', '6ATOB001DEN', 100, ' Áo Thun Giữ Nhiệt Bền Màu, Co Dãn', 99000,
        'https://product.hstatic.net/200000053174/product/74_7e1327814e274d7db452853dd788b8f8_master.jpg',
        'P2', now(), now());

INSERT INTO product_variant (id, sku, quantity, name, price, image, product_id, created_date, last_modified_date)
VALUES ('PV3', '7QAUB003DEN', 100, ' Quần Âu Chống Nhăn, Co Giãn, Bền Màu, Giữ Phom Dáng', 450000,
        'https://product.hstatic.net/200000053174/product/3_2c2fc7fc399a4e3a8dc0fbb442cb2321_master.jpg',
        'P3', now(), now());

INSERT INTO product_variant (id, sku, quantity, name, price, image, product_id, created_date, last_modified_date)
VALUES ('PV4', '6QBDT001XNH', 100, ' Quần Jeans Nam Cao Cấp Bền Màu, Co Dãn, Thấm Hút Tốt', 429000,
        'https://product.hstatic.net/200000053174/product/r__03242_6b41e483de9c43d9bef2f7a1b7073569_master.jpg',
        'P4', now(), now());

--- Variant attribute
INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_1', 'PA_2', 'PV1', 'Xanh');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_2', 'PA_2', 'PV1', 'Be');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_3', 'PA_1', 'PV1', 'S');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_4', 'PA_1', 'PV1', 'M');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_5', 'PA_2', 'PV2', 'Đen');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_6', 'PA_1', 'PV2', 'XL');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_7', 'PA_1', 'PV3', '29');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_8', 'PA_2', 'PV3', 'Đen');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_9', 'PA_1', 'PV4', '30');

INSERT INTO variant_attribute (id, attribute_id, variant_id, value)
VALUES ('PVA_10', 'PA_2', 'PV4', 'Be');

