-- Liquibase formatted SQL
-- ChangeSet Thang:1 labels:initDB,basic-table runOnChange:true splitStatements:false

CREATE TABLE IF NOT EXISTS merchant
(
    id                 character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    store_name         character varying(512),
    url                character varying(255),
    description        text,
    avg_rating         float,
    rating_count       int,
    user_id            character varying(255),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN                DEFAULT TRUE
);

CREATE TABLE IF NOT EXISTS product
(
    id                 CHARACTER VARYING(255) DEFAULT gen_random_uuid()::CHARACTER VARYING(255) PRIMARY KEY,
    name               VARCHAR(255) NOT NULL,
    description        TEXT,
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN                DEFAULT TRUE,
    merchant_id         CHARACTER VARYING(255) DEFAULT NULL,
    CONSTRAINT fK_prod_merchant FOREIGN KEY (merchant_id) REFERENCES merchant (id)
);

CREATE TABLE IF NOT EXISTS category
(
    id                 character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    parent_id          character varying(255),
    name               character varying(255),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN                DEFAULT TRUE,
    CONSTRAINT fk_subcategory FOREIGN KEY (parent_id) REFERENCES category (id)
);

-- Ex: product_variant('uuid', '123', 444, 'quan jean j01', ...)
CREATE TABLE IF NOT EXISTS product_variant
(
    id                 character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    sku                character varying(100),
    quantity           int,
    name               character varying(255),
    price              decimal,
    discount_price     decimal,
    image              character varying(512),
    product_id         character varying(255),
    created_date       TIMESTAMP(6) WITH TIME ZONE,
    last_modified_date TIMESTAMP(6) WITH TIME ZONE,
    active             BOOLEAN                DEFAULT TRUE,
    CONSTRAINT fk_prod_variant FOREIGN KEY (product_id) REFERENCES product (id)
);

-- Ex: product_attribute('uuid', 'kich thuoc')
CREATE TABLE IF NOT EXISTS product_attribute
(
    id   character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    name character varying(255),
    key character varying(255),
    product_id character varying(255),

    CONSTRAINT fk_product_attribute_prod FOREIGN KEY (product_id) REFERENCES product (id)
);

-- Ex: variant_attribute('Size', 'quan jogger', 'M')
CREATE TABLE IF NOT EXISTS variant_attribute
(
    id           character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    attribute_id character varying(255),
    variant_id   character varying(255),
    value        varchar(150),
    CONSTRAINT fk_variant_att_att FOREIGN KEY (attribute_id) REFERENCES product_attribute (id),
    CONSTRAINT fk_variant_att_variant FOREIGN KEY (variant_id) REFERENCES product_variant (id)
);

CREATE TABLE IF NOT EXISTS product_category
(
    id          character varying(255) DEFAULT gen_random_uuid()::CHARACTER varying(255) PRIMARY KEY,
    product_id  character varying(255),
    category_id character varying(255),
    CONSTRAINT fk_prod_cate_prod FOREIGN KEY (product_id) REFERENCES product (id),
    CONSTRAINT fk_prod_cate_cate FOREIGN KEY (category_id) REFERENCES category (id)
);