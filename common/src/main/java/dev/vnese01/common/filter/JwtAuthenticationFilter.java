package dev.vnese01.common.filter;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import dev.vnese01.common.context.JwtContext;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.service.JwtService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
    @Autowired
    private JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
           String authHeader = request.getHeader("Authorization");
           if (StringUtils.isEmpty(authHeader) || !StringUtils.startsWithIgnoreCase(authHeader, "Bearer")) {
               LOGGER.info("No token is provided");
               filterChain.doFilter(request, response);
               return;
           }
           String token = authHeader.substring(7);
           DecodedJWT decodedJWT = jwtService.verify(token);
           JwtTokenDto jwtTokenDto = jwtService.retrieveInfoFromToken(decodedJWT);
           if (jwtTokenDto != null) {
               UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                       jwtTokenDto.getEmail(), null, getAuthorities(jwtTokenDto)
               );
               auth.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
               SecurityContextHolder.getContext().setAuthentication(auth);
               JwtContext.getInstance().setJwt(jwtTokenDto);
            }

           filterChain.doFilter(request, response);
       }catch (BadCredentialsException | JWTVerificationException e) {
           LOGGER.info("Error when verifying token: " + e.getMessage());
       }finally {
            JwtContext.getInstance().removeJwt();
        }
    }

    private Set<GrantedAuthority> getAuthorities(JwtTokenDto jwtTokenDto) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        authorities.addAll(jwtTokenDto.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getId())).toList());
        return authorities;
    }
}
