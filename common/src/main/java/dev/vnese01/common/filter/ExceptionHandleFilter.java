package dev.vnese01.common.filter;

import dev.vnese01.common.ex.InvalidToken;
import dev.vnese01.common.ex.ResourceNotFound;
import dev.vnese01.common.ex.ServiceProcessingException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import java.io.IOException;
@Component
public class ExceptionHandleFilter extends OncePerRequestFilter {
    private final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandleFilter.class);
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        }catch (ServiceProcessingException ex1) {
            LOGGER.info("An error occurs in filter --- >> passing it to global exception handler");
            resolver.resolveException(request, response, null, ex1);
        }catch (ResourceNotFound ex2) {
            LOGGER.info("Resource Not Found Ex --- >> passing it to global exception handler");
            resolver.resolveException(request, response, null, ex2);
        }catch (Exception ex3) {
            LOGGER.info("Some thing went wrong --- >> passing it to global exception handler");
            resolver.resolveException(request, response, null, ex3);
        }
    }
}
