package dev.vnese01.common.repository;

import dev.vnese01.common.model.Permission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends CommonResourceRepository<Permission, String> {
    @Query("select p from Permission p JOIN FETCH p.listRoles r where r.id = ?1 and p.resource = ?2")
    List<Permission> findAllByIdInAndResource(String id, String resource);
}
