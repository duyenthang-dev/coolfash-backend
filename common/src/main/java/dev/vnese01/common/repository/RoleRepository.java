package dev.vnese01.common.repository;

import dev.vnese01.common.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CommonResourceRepository<Role, String> {


}
