package dev.vnese01.common.rest.interceptor;

import dev.vnese01.common.context.JwtContext;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.enums.CommonConstants;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.http.HttpHeaders;

public class AuthFeignInterceptor implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
        JwtTokenDto jwtTokenDto = JwtContext.getInstance().getJwt();
        if (jwtTokenDto != null) {
            template.header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtTokenDto.getOriginalToken());
        }
        template.header("x-internal-key", CommonConstants.INTERNAL_API_KEY);
    }
}
