package dev.vnese01.common.rest;

import dev.vnese01.common.config.GenericFeignConfig;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${feign.permission.name}", url = "${feign.permission.url}", configuration = GenericFeignConfig.class)
@ConditionalOnProperty(prefix = "feign.permission", name = "name")
public interface PermissionClient {
    @GetMapping("/permission/by-role/{roleId}")
    Object loadPermissionByRole(@PathVariable String roleId);
}
