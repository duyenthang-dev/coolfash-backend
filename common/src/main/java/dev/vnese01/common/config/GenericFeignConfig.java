package dev.vnese01.common.config;

import dev.vnese01.common.rest.interceptor.AuthFeignInterceptor;
import org.springframework.context.annotation.Bean;

public class GenericFeignConfig {
    @Bean
    public AuthFeignInterceptor authFeignInterceptor() {
        return new AuthFeignInterceptor();
    }
}
