package dev.vnese01.common.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "dev.vnese01.security.jwt")
@Data
public class JwtConfiguration {
    private long expiration = 3600;
    private int maxLoginFailAllowed;
    private String secret;
}
