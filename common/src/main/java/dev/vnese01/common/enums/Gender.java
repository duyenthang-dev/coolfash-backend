package dev.vnese01.common.enums;

public enum Gender {
    MALE,
    FEMALE,
    UNKNOWN
}
