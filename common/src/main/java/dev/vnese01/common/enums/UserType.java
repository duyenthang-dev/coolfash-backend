package dev.vnese01.common.enums;

public enum UserType {
    BUYER,
    SELLER,
    STAFF,
    ADMIN,
    SUPERADMIN
}
