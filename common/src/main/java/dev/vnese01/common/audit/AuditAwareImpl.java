package dev.vnese01.common.audit;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("auditAwareImpl")
public class AuditAwareImpl implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        String author = SecurityContextHolder.getContext().getAuthentication().getName();
        if (author.isBlank()) {
            author = "Anonymous";
        }
        return Optional.of(author);
    }
}
