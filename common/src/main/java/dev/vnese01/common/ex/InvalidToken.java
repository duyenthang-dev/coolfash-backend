package dev.vnese01.common.ex;

import java.io.Serial;

public class InvalidToken extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -5555222211L;
    public InvalidToken(String message) {
        super(message);
    }
}
