package dev.vnese01.common.ex;

import java.io.Serial;

public class ServiceProcessingException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -5555222211L;
    public ServiceProcessingException(String message) {
        super(message);
    }
}
