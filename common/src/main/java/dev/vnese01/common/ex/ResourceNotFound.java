package dev.vnese01.common.ex;

import java.io.Serial;

public class ResourceNotFound extends RuntimeException{
    @Serial
    private static final long serialVersionUID = 8566508010690025030L;
    public ResourceNotFound(String message, Throwable origin) {
        super(message, origin);
    }

    public ResourceNotFound(String message) {
        super(message);
    }
}
