package dev.vnese01.common.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
@ReadingConverter
public class LocalDateTimeToZoneDateTimeConverter implements Converter<LocalDateTime, ZonedDateTime>, GenericConverter {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    @Override
    public ZonedDateTime convert(LocalDateTime date) {
        logger.info("Apply LocalDateTimeToZoneDateTimeConverter");
        return ZonedDateTime.of(date, ZoneId.systemDefault());
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        var result = new HashSet<ConvertiblePair>();
        result.add(new ConvertiblePair(LocalDateTime.class, ZonedDateTime.class));
        return result;
    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        return convert((LocalDateTime) source);
    }
}
