package dev.vnese01.common.converter;

import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.GenericConverter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.stereotype.Component;

import java.time.*;
import java.util.HashSet;
import java.util.Set;
@Component
@ReadingConverter
public class LocalDateToZoneDateTimeConverter implements Converter<LocalDate, ZonedDateTime>, GenericConverter {
    @Override
    public ZonedDateTime convert(LocalDate source) {
        return ZonedDateTime.of(source, LocalTime.MIDNIGHT, ZoneId.systemDefault());
    }

    @Override
    public Set<ConvertiblePair> getConvertibleTypes() {
        var result = new HashSet<ConvertiblePair>();
        result.add(new ConvertiblePair(LocalDate.class, ZonedDateTime.class));
        return result;

    }

    @Override
    public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
        return convert((LocalDate) source);
    }
}
