package dev.vnese01.common.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

@Component
public class OrderListConverter implements Converter<String, List<Sort.Order>>, Formatter<List<Sort.Order>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderListConverter.class);
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public List<Sort.Order> convert(String source) {
        List<Sort.Order> orders = new ArrayList<>();
        if (source == null || source.isEmpty()) {
            return orders;
        }
        Arrays.stream(source.split(",")).forEach(o -> {
            Sort.Order order = null;
            if (o.charAt(0) == '-') {
                order = Sort.Order.desc(o.substring(1));
            }
            else {
                order = Sort.Order.asc(o);
            }
            orders.add(order);
        });
        return orders;
    }

    @Override
    public List<Sort.Order> parse(String text, Locale locale) throws ParseException {
        LOGGER.info("Calling custom formatter to parse [{}] to List<Order>! Locale ignored!", text);
        return convert(text);
    }

    @Override
    public String print(List<Sort.Order> object, Locale locale) {
        LOGGER.info("Calling custom formatter for List<Order> {}! Locale ignored!", object);
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            LOGGER.error("Error", e);
            return "[]";
        }
    }
}
