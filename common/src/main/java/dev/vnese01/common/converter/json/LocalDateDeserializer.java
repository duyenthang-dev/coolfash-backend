package dev.vnese01.common.converter.json;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@EqualsAndHashCode(callSuper = true)
@Data
@ConditionalOnProperty(prefix = "spring.jackson-custom.serialization", name = "local-date-format")
@Component
public class LocalDateDeserializer extends JsonDeserializer<LocalDate> {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private DateTimeFormatter dateTimeFormatter;

    public LocalDateDeserializer(@Value("${spring.jackson-custom.serialization.local-date-format}") String dateTimeFormat,
                                     @Autowired ObjectMapper objectMapper) {
        logger.info("Applied {} format: {}", LocalDate.class.getSimpleName(), dateTimeFormat);
        dateTimeFormatter = DateTimeFormatter.ofPattern(dateTimeFormat);
        SimpleModule simpleModule = new SimpleModule(LocalDate.class.getSimpleName() + "DeserializerModule",
                new Version(1, 0, 0, null, "", ""));
        simpleModule.addDeserializer(LocalDate.class, this);
        objectMapper.registerModule(simpleModule);

    }
    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        String dateString = jsonParser.getText();
        logger.info("Parsing {} to {}", dateString, LocalDate.class);
        try {
            return LocalDate.parse(dateString, dateTimeFormatter);
        } catch (DateTimeParseException e) {
            logger.debug("Err for " + dateTimeFormatter, e);
            return LocalDate.parse(dateString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }catch (Exception ex) {
            logger.info("deserialize error");
            return LocalDate.parse(dateString, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        }
    }
}
