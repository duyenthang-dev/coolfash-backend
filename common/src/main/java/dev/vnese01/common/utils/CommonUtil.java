package dev.vnese01.common.utils;

import dev.vnese01.common.model.GenericModel;
import dev.vnese01.common.service.CommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import java.beans.FeatureDescriptor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.stream.Stream;

@Component
public class CommonUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);
    public Class<?> findEntityTypeFromCommonService(Class<?> serviceClass) {
        LOGGER.info("Finding entity type from service {}", serviceClass.getName());
        Type superClass = serviceClass.getGenericSuperclass();
        if (superClass instanceof ParameterizedType) {
            return extractGenericType((ParameterizedType) superClass);
        }else {
            Type[] interfaces = serviceClass.getGenericInterfaces();
            LOGGER.info("Continuing with support interfaces {}", Arrays.toString(interfaces));
            for (var type: interfaces) {
                if (type instanceof ParameterizedType) {
                    var rawType = ((ParameterizedType) type).getRawType();
                    if (CommonService.class.isAssignableFrom((Class<?>) rawType)) {
                        LOGGER.info("Extracting entity type...");
                        return extractGenericType((ParameterizedType) type);
                    }
                    else {
                        LOGGER.info("Ignore [{}] cuz it is not CommonService", type);
                    }
                }else {
                    if (type instanceof Class<?> typ){
                        LOGGER.info("Type [{}] is Class. Continue with findEntityTypeFromCommonService...", type);
                        var tmp = findEntityTypeFromCommonService(typ);
                        if (tmp != null) {
                            return tmp;
                        }
                    }
                    else {
                        LOGGER.info("Ignore unknown type [{}]", type);
                    }
                }
            }
        }
        return null;
    }

    private Class<?> extractGenericType(ParameterizedType type) {
        var argumentTypes = type.getActualTypeArguments();
        LOGGER.info("Actual argument type: {}", Arrays.toString(argumentTypes));
        for (var t: argumentTypes) {
            if (t instanceof Class<?> && GenericModel.class.isAssignableFrom((Class<?>) t)) {
                return (Class<?>) t;
            }
        }
        return null;
    }

    public String[] getNullProperties(Object source) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors()).map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null).toArray(String[]::new);
    }
}
