package dev.vnese01.common.service;

import dev.vnese01.common.context.ApplicationContextHolder;
import dev.vnese01.common.ex.ResourceNotFound;
import dev.vnese01.common.ex.ServiceProcessingException;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.common.model.GenericModel;
import dev.vnese01.common.repository.CommonResourceRepository;
import dev.vnese01.common.utils.CommonUtil;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.*;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public interface CommonService <I extends Serializable, T extends GenericModel<I>, R extends CommonResourceRepository<T, I>>{
    /**
     * Get current repository
     * @return repository that service manage
     */
    R getRepository();


    /**
     * Support for create or update entity. All props which are matched name and type already copied. <br />
     * Only do addition copy custom field which is not matched property name with entity
     * */
    default <D extends CreateUpdateDto<I>> void copyAdditionalPropsFromDtoToEntity(D dto, T entity) {

    }

    default <D extends CreateUpdateDto<I>> void doAfterCreateEntity(D dto, T entity) {}

    default <D extends CreateUpdateDto<I>> T createEntity(D dto) {
        var logger = LoggerFactory.getLogger(getClass());
        logger.info("Entering create Entity method...");
        var repository = getRepository();
        T entity;
        entity = createEntityObject();
        return copyAndSaveEntity(dto, entity, repository);
    }

    default <D extends CreateUpdateDto<I>> T updateEntity(D dto) {
        var logger = LoggerFactory.getLogger(getClass());
        logger.info("Entering update Entity method...");
        var repository = getRepository();
        Optional<T> optional = repository.findById(dto.getId());
        T entityDb = optional.orElseThrow(() -> new ResourceNotFound("Entity not found!!"));
        return copyAndSaveEntity(dto, entityDb, repository);
    }

    default Boolean deleteEntity(I entityId) {
        var logger = LoggerFactory.getLogger(getClass());
        logger.info("Entering delete Entity method...");
        var repository = getRepository();
        Optional<T> optional = repository.findById(entityId);
        if (optional.isPresent()) {
            T entity = optional.get();
            repository.delete(entity);
            return true;
        }
        return false;
    }

    default T getEntityDetail(I entityId) {
        var repository = getRepository();
        Optional<T> optional = repository.findById(entityId);
        T entity = optional.orElseThrow(() -> new ResourceNotFound("Entity not found!!"));
        return entity;
    }

    private  <D extends CreateUpdateDto<I>> T copyAndSaveEntity(D dto, T entity, R repository) {
        var logger = LoggerFactory.getLogger(getClass());
        var ctx = ApplicationContextHolder.getInstance().getApplicationContext();
        var commonUtilBean = ctx.getBean(CommonUtil.class);
        logger.info("Copy matched props from DTO to Entity ...");
        BeanUtils.copyProperties(dto, entity, commonUtilBean.getNullProperties(dto));
        logger.info("Calling copyAdditionalPropsFromDtoToEntity ...");
        copyAdditionalPropsFromDtoToEntity(dto, entity);
        entity.setId(dto.getId());
        logger.info("Save entity and return {}", entity);
        entity = repository.save(entity);
        doAfterCreateEntity(dto, entity);
        return entity;
    }


    default Page<T> searchData(String searchText, int pageSize, int pageIndex, List<Sort.Order> orders) {
        var logger = LoggerFactory.getLogger(getClass());
        logger.info("search entities");
        var repository = getRepository();
        if (pageSize > 0 && pageIndex >= 0 ) {
            logger.info("get list with pagination");
            PageRequest pageRequest = PageRequest.of(pageIndex, pageSize, Sort.by(orders));
            if (StringUtils.hasText(searchText)) {
                logger.info("Search by page: {}, pageIndex: {} and searchText: {}", pageSize, pageIndex, searchText);
                return searchByTextAndPage(searchText, pageRequest);
            }
            else {
                logger.info("Search by page: {}, pageIndex: {} and without searchText", pageSize, pageIndex);
                return repository.findAll(pageRequest);
            }
        }
        else {
            logger.info("Load all list entity");
            List<T> entities;
            Sort sort = Sort.by(orders);
            if (StringUtils.hasText(searchText)) {
                logger.info("Search by {} and order: {}", searchText, orders);
                entities = searchByTextAndSort(searchText, sort);
            }
            else {
                logger.info("Search by order: {} and without searchText", orders);
                entities = repository.findAll(sort);
            }
            return new PageImpl<>(entities);
        }
    }

    default T createEntityObject() {
        try {
            return findEnityClass().getConstructor().newInstance();
        }catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
            throw new ServiceProcessingException(e.getMessage());
        }
    }

    default Class<T> findEnityClass() {
        var ctx = ApplicationContextHolder.getInstance().getApplicationContext();
        Class<T> tClass = (Class<T>) ctx.getBean(CommonUtil.class).findEntityTypeFromCommonService(getClass());
        return tClass;
    }

    default Page<T> searchByTextAndPage(String searchText, Pageable pageable) {
        Class<?> entityType = findEnityClass();
        var ctx = ApplicationContextHolder.getInstance().getApplicationContext();
        return ctx.getBean(SimpleSearchSupport.class).searchByTextAndPage(entityType, searchText, pageable);
    }

    default List<T> searchByTextAndSort (String searchText, Sort orders) {
        Class<?> entityType = findEnityClass();
        var ctx = ApplicationContextHolder.getInstance().getApplicationContext();
        return ctx.getBean(SimpleSearchSupport.class).searchByTextAndSort(entityType, searchText, orders);
    }

}
    