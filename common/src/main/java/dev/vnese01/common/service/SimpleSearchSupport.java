package dev.vnese01.common.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface SimpleSearchSupport <T>{
    default boolean isSupported(Class<?> clazz) {
        return false;
    }

    /**
     * Search by text and Pageable
     * @param clazz - entity class
     * @param searchText - searchText
     * @param pageable - pageable
     * */
    Page<T> searchByTextAndPage(Class<? extends T> clazz, String searchText, Pageable pageable);

    /**
     * Search by text and Pageable
     * @param clazz - entity class
     * @param searchText - searchText
     * @param sort - sort
     * */
    List<T> searchByTextAndSort(Class<? extends T> clazz, String searchText, Sort sort);
}
