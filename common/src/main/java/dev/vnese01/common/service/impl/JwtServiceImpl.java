package dev.vnese01.common.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.vnese01.common.config.JwtConfiguration;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.ex.InvalidToken;
import dev.vnese01.common.ex.ServiceProcessingException;
import dev.vnese01.common.service.JwtService;
import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
@Component
public class JwtServiceImpl  implements JwtService{
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtServiceImpl.class);
    @Autowired
    private JwtConfiguration jwtConfiguration;
    private static Algorithm algorithm;
    private static JWTVerifier verifier;

    @Autowired
    private ObjectMapper objectMapper;

    public JwtServiceImpl(@Value("${dev.vnese01.security.jwt.secret}") String secret) {
       algorithm = Algorithm.HMAC256(secret);
       verifier = JWT.require(algorithm).withIssuer("auth0").build();
   }
    @Override
    public String sign(Map<String, Serializable> data) {
        algorithm = Algorithm.HMAC256(jwtConfiguration.getSecret());
        Builder builder = JWT.create().withIssuer("auth0");
        for (Map.Entry<String, Serializable> entry: data.entrySet()) {
            addClaim(builder, entry.getKey(), entry.getValue());
        }
        builder.withIssuedAt(new Date());
        builder.withExpiresAt(new Date(System.currentTimeMillis() + jwtConfiguration.getExpiration() * 1000));
        return builder.sign(algorithm);
    }

    @Override
    public DecodedJWT verify(String jwtToken) {
        try {
            if (jwtToken == null) {
                return null;
            }
            DecodedJWT decodedJWT = verifier.verify(jwtToken);
            if (isTokenExpired(decodedJWT)) {
                throw new JWTVerificationException("JWT token is expired on" + decodedJWT.getExpiresAt());
            }
            return decodedJWT;
        } catch (JWTVerificationException e) {
            LOGGER.info(e.getMessage());
            throw new InvalidToken(e.getMessage());
        }
    }

    @Override
    public JwtTokenDto retrieveInfoFromToken(DecodedJWT decodedJWT) throws JsonProcessingException {
        String json = new String(Base64.getDecoder().decode(decodedJWT.getPayload().getBytes()), StandardCharsets.UTF_8);
        JwtTokenDto tokenDto = objectMapper.reader().forType(JwtTokenDto.class).readValue(json);
        return tokenDto;
    }

    @Override
    public Map<String, Serializable> buildLoginDataMap(JwtTokenDto jwtTokenDto) {
        LOGGER.info("Build Login Data Map");
        Map<String, Serializable> map = new HashMap<>();
        Class<?> clazz = jwtTokenDto.getClass();
        putFieldData(jwtTokenDto, clazz, map);
        LinkedList<String> roles = new LinkedList<>(jwtTokenDto.getRoles().stream().map(role -> {
            try {
                return objectMapper.writeValueAsString(role);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList()));
        map.put("roles", roles);
        return map;
    }

    @Override
    public String buildAccessToken(JwtTokenDto jwtTokenDto) {
        LOGGER.info("================ Build Access Token ===================");
        var payload = buildLoginDataMap(jwtTokenDto);
        return sign(payload);
    }

    @Override
    public Map<String, Serializable> buildRefreshTokenDataMap(JwtTokenDto jwtTokenDto, String token) {
        Map<String, Serializable> map = new HashMap<>();
        map.put("userId", jwtTokenDto.getUserId());
        if (token == null || token.isEmpty()) {
            throw new ServiceProcessingException("Build refresh token error - No access token is provided");
        }
        return map;
    }

    private void putFieldData(JwtTokenDto jwtTokenDto, Class<?> clazz, Map<String, Serializable> dataMap) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field: fields) {
            String fieldName = field.getName();
            if (!Modifier.isStatic(field.getModifiers())) {
                try {
                    LOGGER.info("Getting field data {}", fieldName);
                    Object fieldData = PropertyUtils.getNestedProperty(jwtTokenDto, fieldName);
                    if (fieldData != null && Serializable.class.isAssignableFrom(fieldData.getClass())) {
                        LOGGER.info("Put field data {} to sign map", fieldName);
                        dataMap.put(fieldName, (Serializable) fieldData);
                    }
                    else {
                        LOGGER.info("Field data is not Serializable.");
                    }
                }catch (ReflectiveOperationException ex) {
                    throw new ServiceProcessingException("errors.server.invalid_jwt_field"  + ex.getMessage());
                }
            }
        }
    }

    private void addClaim(Builder builder, String key, Serializable value) {
        if (value == null) {
            builder.withClaim(key, (String) null);
            return;
        }
        Class<?> valueClazz = value.getClass();
        if (String.class.isAssignableFrom(valueClazz) || UUID.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, value.toString());
        }
        else if (Integer.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (Integer) value);
        }
        else if (Double.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (Double) value);
        }
        else if (Boolean.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (Boolean) value);
        }
        else if (Long.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (Long) value);
        }
        else if (Date.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (Date) value);
        }
        else if (String[].class.isAssignableFrom(valueClazz)) {
            builder.withArrayClaim(key, (String[]) value);
        }
        else if (Integer[].class.isAssignableFrom(valueClazz)) {
            builder.withArrayClaim(key, (Integer[]) value);
        }
        else if (Long[].class.isAssignableFrom(valueClazz)) {
            builder.withArrayClaim(key, (Long[]) value);
        }
        else if (List.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, (List<?>) value);
        }
        else if (Enum.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, ((Enum<?>) value).name());
        }
        else if (Collection.class.isAssignableFrom(valueClazz)) {
            builder.withClaim(key, ((Collection<?>) value).stream().collect(Collectors.toList()));
        }
    }

    private boolean isTokenExpired(DecodedJWT decodedJWT) {
       Date expiration = decodedJWT.getExpiresAt();
       return expiration.before(new Date());
    }

}
