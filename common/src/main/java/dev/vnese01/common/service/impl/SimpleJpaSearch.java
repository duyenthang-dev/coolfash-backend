package dev.vnese01.common.service.impl;

import dev.vnese01.common.model.GenericModel;
import dev.vnese01.common.service.SimpleSearchSupport;
import jakarta.persistence.EntityManager;
import jakarta.persistence.metamodel.EntityType;
import jakarta.persistence.metamodel.SingularAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class SimpleJpaSearch implements SimpleSearchSupport<GenericModel<?>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleJpaSearch.class);
    @Autowired
    private EntityManager entityManager;
    private Map<EntityType<?>, Set<SingularAttribute<?, ?>>> supportedAttributes;

    public SimpleJpaSearch(@Autowired EntityManager entityManager) {
        this.entityManager = entityManager;
        supportedAttributes = new HashMap<>();
        var entityTypes = entityManager.getMetamodel().getEntities();
        entityTypes.forEach(et -> {
            Class<?> etClass = et.getJavaType();
            LOGGER.info("Scanning attributes of entity class [{}]", etClass.getName());
            Set<SingularAttribute<?, ?>> attributes = (Set<SingularAttribute<?, ?>>) et.getSingularAttributes();
            LOGGER.info("Attributes: {}", attributes);
            Set<SingularAttribute<?, ?>> attributeSet = new HashSet<>();
            for (SingularAttribute<?, ?> attribute: attributes) {
                var attType = attribute.getJavaType();
                if (String.class.isAssignableFrom(attType)) {
                    attributeSet.add(attribute);
                }
            }
            if (!attributeSet.isEmpty()) {
                LOGGER.info("Entity [{}] enable search for {}", et, attributeSet);
                this.supportedAttributes.put(et, attributeSet);
            }
        });
    }

    @Override
    public Page<GenericModel<?>> searchByTextAndPage(Class<? extends GenericModel<?>> clazz, String searchText, Pageable pageable) {
        var sql = buildSearchSql(clazz);
        var countSql = "SELECT count(o) " + sql;
        LOGGER.info("Count SQL {}", countSql);
        sql = appendSortToSqlQuery(sql, pageable.getSort());
        LOGGER.info("Search SQL: {}", sql);
        var countQuery = entityManager.createQuery(countSql, Long.class);
        countQuery.setParameter("searchText", "%" + searchText +"%");
        long count = countQuery.getSingleResult();

        var query = entityManager.createQuery(sql, clazz);
        query.setFirstResult((int) pageable.getOffset());
        query.setMaxResults(pageable.getPageSize());
        query.setParameter("searchText", "%" + searchText +"%");
        var list = (List<GenericModel<?>>) query.getResultList();

        return new PageImpl<>(list, pageable, count);
    }

    @Override
    public List<GenericModel<?>> searchByTextAndSort(Class<? extends GenericModel<?>> clazz, String searchText, Sort sort) {
        var sql = buildSearchSql(clazz);
        sql = appendSortToSqlQuery(sql, sort);
        LOGGER.info("Search SQL: {}", sql);
        var query = entityManager.createQuery(sql, clazz);
        query.setParameter("searchText", "%" + searchText + "%");

        return (List<GenericModel<?>>) query.getResultList();
    }

    private String buildSearchSql(Class<?> clazz) {
        StringBuilder sqlBuilder = new StringBuilder("FROM ").append(clazz.getSimpleName()).append(" o where ");
        // Find entity matched with clazz from set supportedAttributes
        var optional = supportedAttributes.entrySet().stream()
                .filter(et -> et.getKey().getJavaType().isAssignableFrom(clazz))
                .findFirst();
        int index = 0;
        var et = optional.get();
        var attributes = et.getValue();
        // get all attributes of entity and build search based on those
        for (var attribute: attributes) {
            if (index > 0) {
                sqlBuilder.append(" OR ");
            }
            sqlBuilder.append("lower(").append(attribute.getName()).append(")").append(" like lower(:searchText)");
            index++;
        }

        return sqlBuilder.toString();
    }

    private String appendSortToSqlQuery(String sql, Sort sort) {
        StringBuilder sqlSortBuilder = new StringBuilder(sql);
        if (sort != null && sort.isSorted()) {
            sqlSortBuilder.append(" ORDER BY");
            boolean firstSort = true;
            for (Sort.Order order: sort) {
                if (!firstSort) {
                    sqlSortBuilder.append(",");
                }
                else {
                    sqlSortBuilder.append(" ");
                }
                sqlSortBuilder.append(order.getProperty()).append(" ").append(order.getDirection());
                firstSort = false;
            }
        }

        return sqlSortBuilder.toString();
    }

}
