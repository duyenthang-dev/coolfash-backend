package dev.vnese01.common.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import dev.vnese01.common.dto.JwtTokenDto;

import java.io.Serializable;
import java.util.Map;

public interface JwtService {
    String sign(Map<String, Serializable> data);
    DecodedJWT verify(String jwtToken);
    JwtTokenDto retrieveInfoFromToken(DecodedJWT decodedJWT) throws JsonProcessingException;
    Map<String, Serializable> buildLoginDataMap(JwtTokenDto jwtTokenDto);
    public String buildAccessToken(JwtTokenDto jwtTokenDto);
    default String buildRefreshToken(JwtTokenDto jwtTokenDto, String token) {
        return sign(buildRefreshTokenDataMap(jwtTokenDto, token));
    }
    Map<String, Serializable> buildRefreshTokenDataMap(JwtTokenDto jwtTokenDto, String token);
}
