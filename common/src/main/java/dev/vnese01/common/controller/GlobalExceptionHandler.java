package dev.vnese01.common.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.vnese01.common.dto.CommonResponse;
import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.ex.InvalidToken;
import dev.vnese01.common.ex.ResourceNotFound;
import dev.vnese01.common.ex.ServiceProcessingException;
import feign.FeignException;
import feign.RetryableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.resource.NoResourceFoundException;

import java.sql.SQLException;
import java.util.Map;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @Autowired
    private ObjectMapper objectMapper;

    @ExceptionHandler(ResourceNotFound.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Object handleResourceNotFound(Exception e) {
        LOGGER.debug(e.getMessage(), e);
        return GeneralApiResponse.createErrorResponse("resource_not_found", e.getMessage());
    }

    @ExceptionHandler(ServiceProcessingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleServiceProcessingException(Exception e) {
        LOGGER.debug(e.getMessage(), e);
        return GeneralApiResponse.createErrorResponse("service_processing_fail", e.getMessage());
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public Object handleAccessDeniedException(Exception e) {
        LOGGER.debug(e.getMessage(), e);
        return GeneralApiResponse.createErrorResponse("access_denied", e.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers,
                                                                   HttpStatusCode status, WebRequest request) {
        ServletWebRequest webRequest = (ServletWebRequest) request;
        String message = "Endpoint not found: " + webRequest.getRequest().getRequestURI();
        var response = new GeneralApiResponse<>(message);
        return this.handleExceptionInternal(ex, response,  headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleNoResourceFoundException(NoResourceFoundException ex, HttpHeaders headers,
                                                                   HttpStatusCode status, WebRequest request) {
        ServletWebRequest webRequest = (ServletWebRequest) request;
        String message = "Endpoint not found: " + webRequest.getRequest().getRequestURI();
        var response = new GeneralApiResponse<>(message);
        return this.handleExceptionInternal(ex, response,  headers, status, request);
    }

    @ExceptionHandler(SQLException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handlePsqlException(SQLException ex) {
        final String sqlState = ex.getSQLState();
        switch (sqlState) {
            case "23505" -> {
                String msg = ex.getLocalizedMessage().split("Details: ")[1];
                return GeneralApiResponse.createErrorResponse("unique_violation", msg);
            }
            default -> {
                return GeneralApiResponse.createErrorResponse("sql_error", "Error in persistence layer");
            }
        }
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public Object handleBadCredentialsException(BadCredentialsException ex) {
        String msg = ex.getMessage();
        LOGGER.error(msg);
        return GeneralApiResponse.createErrorResponse("authentication_fail", "Password not matched");
    }

    @ExceptionHandler(InvalidToken.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleInvalidToken(InvalidToken ex) {
        String msg = ex.getMessage();
        LOGGER.error(msg);
        return GeneralApiResponse.createErrorResponse("invalid_access_token", msg);
    }

    @ExceptionHandler(RetryableException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object handleRetryableException(RetryableException ex) {
        String msg = ex.getMessage();
        return GeneralApiResponse.createErrorResponse("feign_exception", msg);
    }

    @ExceptionHandler(FeignException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Object handleFeignException(FeignException exception) {
        String msg = exception.getMessage();
        msg = msg.substring(1, msg.length() -1);
        CommonResponse response = null;
        try {
            response = objectMapper.readValue(msg, CommonResponse.class);
        } catch (JsonProcessingException e) {
            return GeneralApiResponse.createErrorResponse("cannot_read_feign_ex", msg);
        }
        return GeneralApiResponse.createErrorResponse(response.getStatusCode(), response.getResult());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object handleException(Exception ex) {
        String msg = ex.getMessage();
        LOGGER.info(msg);
        ex.printStackTrace();
        return GeneralApiResponse.createErrorResponse("internal_server_error", msg);
    }
}
