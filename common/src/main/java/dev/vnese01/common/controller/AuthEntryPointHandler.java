package dev.vnese01.common.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import dev.vnese01.common.dto.GeneralApiResponse;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import java.io.IOException;

public class AuthEntryPointHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(403);
        var res = GeneralApiResponse.createErrorResponse("access_denied", "Access token required");
        String json = new ObjectMapper().writeValueAsString(res);
        response.getWriter().write(json);
    }
}
