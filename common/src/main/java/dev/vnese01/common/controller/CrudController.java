package dev.vnese01.common.controller;

import dev.vnese01.common.dto.GeneralApiResponse;
import dev.vnese01.common.dto.PageDto;
import dev.vnese01.common.model.CreateUpdateDto;
import dev.vnese01.common.model.GenericModel;
import dev.vnese01.common.service.CommonService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

public interface CrudController <I extends Serializable, T extends GenericModel<I>, D extends CreateUpdateDto<I>>{
    CommonService<I, T, ?> getHandlingService();
    default Converter<T, ?> getEntityToDtoConverter() {
        return null;
    }

    @PostMapping(value = "")
    default PageDto<?> search(@RequestParam (value = "pageSize", required = false, defaultValue = "10") int pageSize,
                              @RequestParam(value = "pageIndex", required = false, defaultValue = "0") int pageIndex,
                              @RequestParam(value = "orders", required = false, defaultValue = "")List<Sort.Order> orders,
                              @RequestParam(value = "searchText", required = false, defaultValue = "") String searchText
                              ) {
        var converter = getEntityToDtoConverter();
        var result = getHandlingService().searchData(searchText, pageSize, pageIndex, orders);
        if (converter == null) {
            return PageDto.createSuccessfulResponse(result);
        }
        return PageDto.createSuccessfulResponse(result.map(r -> converter.convert(r)));
    }

    @PostMapping(value = "/create")
    default GeneralApiResponse<?> createEntity(@RequestBody D entity) {
//        entity.setId(null);
        var result = getHandlingService().createEntity(entity);
        var converter = getEntityToDtoConverter();
        if (converter == null) {
            return GeneralApiResponse.createSuccessResponse(result);
        }
        return GeneralApiResponse.createSuccessResponse(converter.convert(result));
    }

    @PatchMapping(value = "/{entityId}")
    default GeneralApiResponse<?> updateEntity(@PathVariable("entityId") I entityId, @RequestBody D entity) {
        entity.setId(entityId);
        var result = getHandlingService().updateEntity(entity);
        var converter = getEntityToDtoConverter();
        if (converter == null) {
            return GeneralApiResponse.createSuccessResponse(result);
        }
        return GeneralApiResponse.createSuccessResponse(converter.convert(result));
    }

    @DeleteMapping(value = "/{entityId}")
    default GeneralApiResponse<Boolean> deleteEntity(@PathVariable("entityId") I entityId) {
        return GeneralApiResponse.createSuccessResponse(getHandlingService().deleteEntity(entityId));
    }

    @GetMapping(value = "/{entityId}")
    default GeneralApiResponse<?> getDetails(@PathVariable("entityId") I entityId) {
        T result = getHandlingService().getEntityDetail(entityId);
        var converter = this.getEntityToDtoConverter();
        if (converter == null) {
            return GeneralApiResponse.createSuccessResponse(result);
        }
        return GeneralApiResponse.createSuccessResponse(converter.convert(result));
    }
}
