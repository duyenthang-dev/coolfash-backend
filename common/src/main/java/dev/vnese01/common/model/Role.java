package dev.vnese01.common.model;

import jakarta.persistence.*;
import lombok.*;
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@RequiredArgsConstructor
@Table(name = "role")
public class Role extends BaseEntity implements GenericModel<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String name;
    private String description;
}
