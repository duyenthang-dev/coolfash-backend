package dev.vnese01.common.model;

import org.springframework.data.annotation.Id;

import java.io.Serializable;

public interface GenericModel <T extends Serializable> extends Serializable{
    @Id
    T getId();
    void setId(T id);
}

