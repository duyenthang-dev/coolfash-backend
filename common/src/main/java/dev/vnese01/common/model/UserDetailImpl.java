package dev.vnese01.common.model;

import dev.vnese01.common.dto.UserFeignResponse;
import dev.vnese01.common.dto.UserRoleDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class UserDetailImpl implements UserDetails {
    private UserFeignResponse user;
    private Collection<? extends GrantedAuthority> authorities;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    private static Set<GrantedAuthority> convertAuthorities(Set<UserRoleDto> roleSet) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        for (UserRoleDto role: roleSet) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }
        return authorities;
    }

    public static UserDetailImpl buildUser(UserFeignResponse user) {
        return new UserDetailImpl(user, convertAuthorities(user.getRoles()));
    }


}
