package dev.vnese01.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Transient;

import java.io.Serializable;

public interface CreateUpdateDto <T extends Serializable> {
//    @Transient
//    @org.springframework.data.annotation.Transient
//    @JsonIgnore
    default T getId() {
        if (GenericModel.class.isAssignableFrom(getClass())) {
            GenericModel<T> genericModel = (GenericModel<T>) this;
            return genericModel.getId();
        }
        throw new UnsupportedOperationException("Please override getId() for custom CreateUpdateDto!");
    }

//    @Transient
//    @org.springframework.data.annotation.Transient
//    @JsonIgnore
    default void setId(T id) {
        if (GenericModel.class.isAssignableFrom(getClass())) {
            GenericModel<T> genericModel = (GenericModel<T>) this;
            genericModel.setId(id);
        }
        throw new UnsupportedOperationException("Please override setEntityId() for custom CreateUpdateDto!");
    }
}
