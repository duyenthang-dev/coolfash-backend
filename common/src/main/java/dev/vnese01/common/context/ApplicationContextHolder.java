package dev.vnese01.common.context;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@Getter
@Setter
public class ApplicationContextHolder implements ApplicationContextAware {
    private static ApplicationContextHolder INSTANCE = new ApplicationContextHolder();
    private ApplicationContext applicationContext;
    private ApplicationContextHolder() {

    }
    public static ApplicationContextHolder getInstance() {
        return INSTANCE;
    }
}
