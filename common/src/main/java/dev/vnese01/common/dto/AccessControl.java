package dev.vnese01.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccessControl {
    private String resourceName;
    private String action;
}
