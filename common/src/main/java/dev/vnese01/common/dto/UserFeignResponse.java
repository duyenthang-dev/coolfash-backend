package dev.vnese01.common.dto;

import dev.vnese01.common.enums.Gender;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Data
@RequiredArgsConstructor
public class UserFeignResponse {
    private String id;
    private String avatar;
    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private LocalDate birthday;
    private Gender gender;
    Set<UserRoleDto> roles;
    private List<Address> addressList = new LinkedList<>();

    @Data
    @RequiredArgsConstructor
    public static class Address {
        private String street;
        private String ward;
        private String district;
        private String country;
        private int zipcode;
        private String representName;
        private String phoneNumber;
        private boolean isDefault;
    }
}
