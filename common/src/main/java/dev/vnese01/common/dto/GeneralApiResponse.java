package dev.vnese01.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneralApiResponse<T> implements Serializable {
    private static final long serialUID = 12356L;
    public static int STATUS_SUCCESS = 1;
    public static int STATUS_ERROR = 0;
    public static int STATUS_UNKNOWN_ERROR = -1;
    private String statusCode;
    private int status;
    private T result;
    public GeneralApiResponse(T result) {
        this.result = result;
        this.statusCode = "";
        this.status = STATUS_SUCCESS;
    }

    public static <T> GeneralApiResponse<T> createSuccessResponse(T result) {
        return new GeneralApiResponse<T>("OK", STATUS_SUCCESS, result);
    }

    public static GeneralApiResponse<String> createErrorResponse(String statusCode, String message) {
        return new GeneralApiResponse<String>(statusCode, STATUS_ERROR, message);
    }
}
