package dev.vnese01.common.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.vnese01.common.ex.ServiceProcessingException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRoleDto implements Serializable {
    private String id;
    private String name;
    private String description;

    public UserRoleDto(String json) {
        try {
            UserRoleDto that = new ObjectMapper().readValue(json, getClass());
            this.id = that.id;
            this.name = that.name;
            this.description = that.description;
        } catch (JsonProcessingException e) {
            throw new ServiceProcessingException("Parse json error");
        }
    }
}
