package dev.vnese01.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.io.Serial;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageDto<T> extends GeneralApiResponse<List<T>>{
    @Serial
    private static final long serialVersionUID = 3086186715911718179L;
    private Sort sort;
    private int pageSize;
    private int pageNumber;
    private long totalElements;
    private int totalPages;

    public PageDto(String statusCode, int status, Page<T> page) {
        super(statusCode, status, page.getContent());
        setPageDate(page);
    }

    private void setPageDate(Page<T> page) {
        setResult(page.getContent());
        setPageNumber(page.getNumber());
        setTotalPages(page.getTotalPages());
        setTotalElements(page.getTotalElements());
        setSort(page.getSort());
    }
    public boolean isEmpty() {
        return getResult().isEmpty();
    }

    public boolean isHasNext() {
        return pageNumber < totalPages - 1;
    }

    public boolean isHasPrevious() {
        return pageNumber > 0;
    }

    public static <T> PageDto<T> createSuccessfulResponse(Page<T> result) {
        return new PageDto<>("OK", 200, result);
    }

}
