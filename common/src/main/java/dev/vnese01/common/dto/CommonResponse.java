package dev.vnese01.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResponse implements Serializable {
    private String statusCode;
    private int status;
    private String result;
}
