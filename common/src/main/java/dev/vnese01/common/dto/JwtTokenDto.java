package dev.vnese01.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedList;

@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class JwtTokenDto implements Serializable {
    private String originalToken;
    private String userId;
    private String username;
    private String email;
    private Collection<UserRoleDto> roles;

    public JwtTokenDto(String userId, String username, String email) {
        this.userId = userId;
        this.username = username;
        this.email = email;
        this.roles = new LinkedList<>();
    }
}
