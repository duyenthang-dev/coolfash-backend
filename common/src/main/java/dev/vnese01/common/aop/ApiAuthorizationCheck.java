package dev.vnese01.common.aop;

import dev.vnese01.common.annotation.ControllerManagedResource;
import dev.vnese01.common.context.JwtContext;
import dev.vnese01.common.dto.AccessControl;
import dev.vnese01.common.dto.JwtTokenDto;
import dev.vnese01.common.enums.CommonConstants;
import dev.vnese01.common.model.Permission;
import dev.vnese01.common.repository.PermissionRepository;
import jakarta.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Note: To enable auto config from yml file, you need to impl getter, setter and constructor
 * @apiNote An AOP which retrieve permission from database and check authorization
 * @author duyenthang <br />
 */

@Aspect
@Component
@EnableConfigurationProperties
@Data
@NoArgsConstructor
@AllArgsConstructor
@Configuration
@ConfigurationProperties(prefix = "dev.vnese01.author-check")
public class ApiAuthorizationCheck {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private PermissionRepository permissionRepository;
    private List<AccessControl> defaultControl = new ArrayList<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(ApiAuthorizationCheck.class);
    private static final String EXECUTION_FEIGN_CLIENT = "!execution(* dev.vnese01.coolfash.auth.rest.*Client.*(..))";
    private static final String MANUAL_CHECK_EXCLUDED = "!@annotation(dev.vnese01.common.annotation.ManualPermissionCheck)";
    private static final String BYPASS_AUTO_AUTHOR_CHECK = MANUAL_CHECK_EXCLUDED + " && " + EXECUTION_FEIGN_CLIENT;
    @Around("@annotation(org.springframework.web.bind.annotation.GetMapping) && " + BYPASS_AUTO_AUTHOR_CHECK)
    public Object verifyGetRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("Auto check authorization for get request {}", request.getRequestURI());
        return verifyRequest(joinPoint, RequestMethod.GET);
    }

    @Around("@annotation(org.springframework.web.bind.annotation.PostMapping) && " + BYPASS_AUTO_AUTHOR_CHECK)
    public Object verifyPostRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("Auto check authorization for post request {}", request.getRequestURI());
        return verifyRequest(joinPoint, RequestMethod.POST);
    }

    @Around("@annotation(org.springframework.web.bind.annotation.PatchMapping) && " + BYPASS_AUTO_AUTHOR_CHECK)
    public Object verifyPatchRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("Auto check authorization for patch request {}", request.getRequestURI());
        return verifyRequest(joinPoint, RequestMethod.PATCH);
    }

    @Around("@annotation(org.springframework.web.bind.annotation.DeleteMapping) && " + BYPASS_AUTO_AUTHOR_CHECK)
    public Object verifyDeleteRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        LOGGER.info("Auto check authorization for delete request {}", request.getRequestURI());
        return verifyRequest(joinPoint, RequestMethod.DELETE);
    }


    public Object verifyRequest(ProceedingJoinPoint joinPoint, RequestMethod requestMethod) throws Throwable {
        if (checkInternalApiCall()) {
            LOGGER.info("Internal call, pass API authorization check");
            return joinPoint.proceed();
        }

        JwtTokenDto jwtTokenDto = JwtContext.getInstance().getJwt();
        if (jwtTokenDto != null) {
            String requestURI = request.getRequestURI();
            var controllerClass = joinPoint.getTarget().getClass();
            var controllerManagedResource = controllerClass.getAnnotation(ControllerManagedResource.class);
            String managedResourceName = controllerManagedResource.value();
            LOGGER.info("Request URI: {}, managedResource: {}", requestURI, managedResourceName);
            List<String> roles = JwtContext.getInstance().getJwt().getRoles().stream()
                    .map(userRole -> userRole.getId()).
                    collect(Collectors.toList());
            boolean isAdmin = false;
            boolean foundMatch = false;
            for(String role: roles) {
                if ("SuperAdmin".equalsIgnoreCase(role)) {
                    isAdmin = true;
                    break;
                }
            }
            if (!isAdmin) {
                for (String role: roles) {
                    if (verifyRole(role, managedResourceName, requestURI, requestMethod.name())) {
                        foundMatch = true;
                    }
                }

                var defaultAction = defaultControl.stream()
                        .filter(action -> action.getResourceName().equalsIgnoreCase(managedResourceName)).collect(Collectors.toList())
                        .get(0)
                        .getAction();

                if (!foundMatch && !defaultAction.equalsIgnoreCase("ALLOW")) {
                    throw new AccessDeniedException("resource_requires_permission_config_to_access");
                }
            }
        }
        return joinPoint.proceed();
    }

    private boolean checkInternalApiCall() {
        String internalApiKey = request.getHeader("x-internal-key");
        return CommonConstants.INTERNAL_API_KEY.equals(internalApiKey);
    }

    private boolean verifyRole(String role, String managedResourceName, String requestUri, String requestMethod) {
        List<Permission> permissions = permissionRepository.findAllByIdInAndResource(role, managedResourceName);
        boolean foundedMatch = false;
        AntPathMatcher pathMatcher = new AntPathMatcher();
        for (Permission permission: permissions) {
            var matchRequestMethod = "ANY".equalsIgnoreCase(permission.getRequestMethod()) ||
                    requestMethod.equalsIgnoreCase(permission.getRequestMethod());
            var matchUrl = "".equalsIgnoreCase(permission.getUrl()) || pathMatcher.match(permission.getUrl(), requestUri);
            if (matchRequestMethod && matchUrl) {
                foundedMatch = true;
                checkPermission(permission, requestUri);
            }
        }
        return foundedMatch;
    }

    private void checkPermission(Permission permission, String requestUri) {
        var action = permission.getAction();
        switch (action.toUpperCase()) {
            case "DENY" -> throw new AccessDeniedException("access_denied_" + permission.getUrl());
            case "ALLOW_SELF" -> {}
        }
    }
}
